(defproject cloudcode "0.1.0-SNAPSHOT"
  :description "a web code editor"
  :url "http://example.com/FIXME"

  :source-paths ["src/clj"]

  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-2371"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [om "0.7.1"]
                 [sablono "0.2.22"]
                 [com.datomic/datomic-free "0.9.4815"]
                 [ring/ring-core "1.3.0"]
                 [ring/ring-devel "1.3.0"]
                 [fogus/ring-edn "0.2.0"]
                 [http-kit "2.1.18"]
                 [compojure "1.1.8"]
                 [com.taoensso/sente "0.15.1"]
                 [cljs-ajax "0.2.6"]
                 [org.clojars.leanpixel/cljs-uuid-utils "1.0.0-SNAPSHOT"]
                 [crypto-password "0.1.3"]
                 [enlive "1.1.5"]]

  :plugins [[com.keminglabs/cljx "0.4.0"]
            [lein-cljsbuild "1.0.3"]]

  :cljx {:builds [{:source-paths ["src/cljx"]
                   :output-path "target/classes"
                   :rules :clj}

                  {:source-paths ["src/cljx"]
                   :output-path "target/generated/cljs"
                   :rules :cljs}]}

  :cljsbuild {:builds
               {:cloudcode
                {:source-paths ["src/cljs" "target/generated/cljs"]
                 :compiler {:output-to "resources/public/script/main.js"
                            :pretty-print true
                            :optimizations :advanced
                            :externs ["react/externs/react.js"]}}}})
