(ns cloudcode.serializers.test-setup
  (:require
    [cloudcode.serializers.revision :refer :all]
    [cloudcode.serializers.project :refer :all]
    [cloudcode.entities.project :as project]
    [cloudcode.entities.revision :as revision]
    [cloudcode.entities.unit :as unit]
    [cloudcode.stores.in-memory :as store-impl]
    [cloudcode.stores.interface :as store-interface]
    [cloudcode.stores.protocol :refer :all]
    [cloudcode.utils.architecture.message-bus :refer :all]))


;; ===== Store setup =====

(def store-state (atom {}))

(def project-store
  (store-impl/create project/spec store-state))

(def unit-store
  (store-impl/create unit/spec store-state))

(def revision-store
  (store-impl/create revision/spec store-state))

(def store-service
  (-> (service "store-service")
      (install-implementation!
        (merge
          (store-interface/create-implementation project-store)
          (store-interface/create-implementation revision-store)
          (store-interface/create-implementation unit-store)))
      (start-service!)))

(def store-bus
  (-> (bus "store-bus")
      (register-service! store-service)))


;; ===== Serializer =====

(def out (bus "out"))

(def serializer
  (-> (service "project serializer")
      (install-implementation!
        (merge
          project-serializer
          revision-serializer))
      (register-output! {:default out
                         :store store-bus})
      (start-service!)))

(def in
  (-> (bus "in")
      (register-service! serializer)))


;; ===== Test data =====

(def test-project1
  {:name "proj1"
   :user-deps
   #{{:project-name "proj2"
      :revision-label "first rev"}
     {:project-name "proj3"
      :revision-label "version-1.2.3"}}})

(def test-revision1
  {:label "v-1.0.0"
   :project "proj1"
   :rev 5
   :interface
   #{{:path "proj1.utils.math.add"
      :source "function (a, b) {return a + b;}"}

     {:path "proj1.utils.math.addAll"
      :dependencies
      #{{:path "proj1.utils.math.add"
        :alias "add"}}
      :source "function (coll) {return coll.reduce(add, 0);}"}}})

(def test-revision2
  {:label "v-0.0.9"
   :project "proj1"
   :rev 3
   :interface
   #{{:path "proj1.utils.math.add"
      :source "function (A, B) {return A + B;}"}

     {:path "proj1.utils.math.addAll"
      :dependencies
      #{{:path "proj1.utils.math.add"
         :alias "add"}}
      :source "function (C) {return C.reduce(add, 0);}"}}})

(def test-revisions
  {:revisions
   #{test-revision1
     test-revision2}})

(def test-projects
  {:projects
   #{test-project1
     {:name "proj2"}
     {:name "proj3"}}})

(def test-data (merge test-revisions test-projects))
