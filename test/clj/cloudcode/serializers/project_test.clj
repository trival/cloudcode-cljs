(ns cloudcode.serializers.project-test
  (:require
    [cloudcode.serializers.test-setup :refer :all]
    [cloudcode.entities.project :refer [spec]]
    [cloudcode.utils.data.model :as m]
    [cloudcode.stores.protocol :refer :all]
    [midje.sweet :refer :all]
    [helpers.async :refer :all]
    [clojure.set :refer [subset?]]
    [cloudcode.utils.architecture.message-bus :refer :all]))


(namespace-state-changes (before :facts (reset! store-state {})))


(fact
  "succeeds on empty"
  (send! in (evt :project.serializer/import {:projects #{}}))
  (check-bus out) => (evt :project.serializer.done/import))


(fact
  "serializer stores processed projects"
  (send! in (evt :project.serializer/import test-projects))
  (check-bus out) => (evt :project.serializer.done/import)

  (let [stored (all project-store)]
    (count stored) => 3

    (doseq [p stored]
      (:name p) => #"proj\d"
      (class (:id p)) => java.util.UUID)

    (let [p (-> (by-values project-store {:name "proj1"})
                (first))]
      (subset? (set test-project1) (set p)) => true)))


(fact
  "returns an error if a project with the same name already exists in store"
  (save project-store #{(m/create-of spec {:name "proj1"})})
  (send! in (evt :project.serializer/import test-projects))
  (let [error (check-bus out)
        data (payload error)]
    (error? error) => true
    (id error) => :project.serializer/import
    (count (:projects data)) => 1
    (:name (first (:projects data))) => "proj1")

  (let [stored (all project-store)]
    (count stored) => 1))
