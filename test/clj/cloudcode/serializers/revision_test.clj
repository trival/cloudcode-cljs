(ns cloudcode.serializers.revision-test
  (:require
    [cloudcode.serializers.test-setup :refer :all]
    [cloudcode.entities.project :as project]
    [cloudcode.entities.revision :as revision]
    [cloudcode.entities.unit :as unit]
    [cloudcode.stores.in-memory :as store-impl]
    [cloudcode.stores.interface :as store-interface]
    [cloudcode.stores.protocol :refer :all]
    [cloudcode.utils.data.model :as m]
    [midje.sweet :refer :all]
    [helpers.async :refer :all]
    [clojure.set :refer [subset?]]
    [cloudcode.utils.architecture.message-bus :refer :all]))


(namespace-state-changes (before :facts (reset! store-state {})))


(defn default-import
  []
  (send! in (evt :project.serializer/import test-projects))
  (check-bus out) => (evt :project.serializer.done/import)

  (send! in (evt :revision.serializer/import test-revisions))
  (check-bus out) => (evt :revision.serializer.done/import))


(fact
  "succeeds on empty"
  (send! in (evt :revision.serializer/import {:revisions #{}}))
  (check-bus out) => (evt :revision.serializer.done/import))


(fact
  "serializer stores processed revisions"
  (default-import)

  (let [stored (all revision-store)]
    (count stored) => 2

    (doseq [r stored]
      (:label r) => #"^v-\d"
      (class (:id r)) => java.util.UUID)))


(fact
  "replaces project name with its store id"
  (default-import)

  (let [p (first (by-values project-store {:name "proj1"}))
        r (first (by-values revision-store {:label "v-0.0.9"}))]
    (:project r) => (:id p)))


(fact
  "stores units separately and sets up proper revision interface"
  (default-import)

  (let [units (all unit-store)
        ifs (-> (by-values revision-store {:label "v-0.0.9"})
                (first)
                (:interface))]
    (count units) => 4

    (set? ifs) => true

    (doseq [i ifs]
      (->> (:unit-id i)
           (hash-set)
           (by-id unit-store)
           (first)
           (:path)) => (:public-path i))))


(fact
  "returns an error if project is not existing"
  (send! in (evt :revision.serializer/import
                 {:revisions #{test-revision1}}))

  (let [error (check-bus out)
        data (payload error)]
    (error? error) => true
    (id error) => :revision.serializer/import
    (:error data) => #"project"
    (first (:revisions data)) => test-revision1)

  (let [stored (all revision-store)]
    (count stored) => 0))


(fact
  "returns an error if revision already exists"
  (default-import)
  (send! in (evt :revision.serializer/import
                 {:revisions #{test-revision1}}))

  (let [error (check-bus out)
        data (payload error)]
    (error? error) => true
    (id error) => :revision.serializer/import
    (:error data) => #"revision"
    (:error data) => #"exists"
    (first (:revisions data)) => test-revision1)

  (let [stored (all revision-store)]
    (count stored) => (count (:revisions test-revisions))))
