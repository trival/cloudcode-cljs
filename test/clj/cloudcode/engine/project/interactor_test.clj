(ns cloudcode.engine.project.interactor-test
  (:require
    [cloudcode.engine.project.interactor :refer :all]
    [midje.sweet :refer :all]
    [helpers.async :refer :all]
    [cloudcode.utils.architecture.message-bus :refer :all]))


(def mock-store-impl
  {:project.store/all
   (fn [in out]
     (send! out (msg :project.store.done/all
                     #{'project1 'project2})))

   :all
   (fn [in out]
     (send! (:test out) in))

   :revision.store/by-values
   (fn [in out]
     (send! out (msg :revision.store.done/by-values #{})))})


(def store-out (bus "bus-out"))


(def mock-store
  (-> (service "mock-store")
      (install-implementation! mock-store-impl)
      (register-output! {:test store-out})
      (start-service!)))


(def store-bus
  (-> (bus)
      (register-service! mock-store)))


(def out (bus "out"))


(defn action1
  [data ps]
  [nil 'project 'revision])


(defn action2
  [data ps]
  ['error])


(defn create-service
  [action]
  (-> (service "project-interactor")
              (install-implementation!
                {:project.action/create (create-handler action)})
              (register-output! {:default out
                                 :store store-bus})
              (start-service!)))

(facts
  "about successfull create-action"
  (let [s (create-service action1)
        in (-> (bus "in")
               (register-service! s))]

    (send! in (evt :project.action/create {:name "newproject"}))
    (let [res (check-bus out)]

      (fact
        "stores new revision new project"
        (payload (check-bus store-out)) => 'revision
        (payload (check-bus store-out)) => 'project)

      (fact
        "returns a project wrapped in metadata"
        (event? res) => true
        (let [[_ {p :project}] (payload res)]
          p => 'project)))))


(facts
  "about error action"
  (let [s (create-service action2)
        in (-> (bus "in")
               (register-service! s))
        m (evt :project.action/create {:name "newproject"})]

    (send! in m)
    (let [res (check-bus out)]
      (error? res) => true
      (:origin (payload res)) => m)))

