(ns cloudcode.engine.project.create-test
  (:require
    [cloudcode.engine.project.create :refer :all]
    [cloudcode.entities.project :as project]
    [cloudcode.entities.revision :as revision]
    [cloudcode.utils.data.model :as m]
    [midje.sweet :refer :all]
    [helpers.async :refer :all]
    [cloudcode.utils.architecture.message-bus :refer :all]))


(fact
  "creates a project and a revision with provided name"
  (let [[e p r] (create {:name "foo"} #{})]

    e => nil
    (m/valid? project/spec p) => true
    (:name p) => "foo"))


(fact
  "creates a revision for the project"
  (let [[e p r] (create {:name "foo"} #{})]
    (m/valid? revision/spec r) => true
    (:active-revision p) => (:id r)
    (:project r) => (:id p)))


(fact
  "returns error if name already present"
   (let [[e p r] (create {:name "foo"} #{{:name "foo"}})]
     p => nil
     r => nil
     e => #"name"
     e => #"foo"))


(fact
  "returns error if name is not defined or empty"
  (let [[e1 p1 r1] (create {:name ""} #{})
        [e2 p2 r2] (create {:name nil} #{})]
    (= p1 p2 r1 r2 nil) => true
    e1 => #"name"
    e2 => #"name"))
