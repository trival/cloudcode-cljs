(ns cloudcode.stores.default-store-test
  (:require
    [cloudcode.stores.interface :refer :all]
    [cloudcode.stores.in-memory :refer :all]
    [midje.sweet :refer :all]
    [helpers.async :refer :all]
    [cloudcode.utils.architecture.message-bus :refer :all]))


(def spec
  {:name "foo"
   :properties
   {:id {:type :uuid
         :identifier? true}
    :name {:type :string
           :required? true}
    :age {:type :long}}})


(def a (atom {}))


(namespace-state-changes (before :facts (reset! a {})))


(fact
  "have key names matching the default keys"
  (let [s(create-implementation (create spec a))]
    (->> s
         (keys)
         (map name)
         (set)) => (set (map name default-keys))))


(fact
  "have key namespace defined by the name of the entity and .store"
  (let [s(create-implementation (create spec a))]
    (->> s
         (keys)
         (map namespace)
         (every? (partial = "foo.store"))) => true))


(def out (bus))


(def foo-store-service
  (-> (service)
      (install-implementation! (create-implementation (create spec a)))
      (register-output! out)
      (start-service!)))


(def in
  (-> (bus)
      (register-service! foo-store-service)))


(def e1 {:id 11 :name "fofo"})
(def e2 {:id 22 :name "bubu" :age 12})
(def e3 {:id 33 :name "fofo" :age 12})
(def e4 {:id 44 :name "fufu" :age 13})
(def e5 {:id 55 :name "bibi" :age 14})
(def e6 {:id 66 :name "lala" :age 99})


(fact
  "can save and retrieve entities"
  (send! in (msg :foo.store/save e1))
  (check-bus out) => (msg :foo.store.done/save #{e1})
  (send! in (msg :foo.store/by-id 11))
  (check-bus out) => (msg :foo.store.done/by-id #{e1}))


(fact
  "can save and retrieve sets of entities"
  (send! in (msg :foo.store/save #{e3 e4 e5}))
  (check-bus out) => (msg :foo.store.done/save #{e3 e4 e5})
  (-> (send! in (req :foo.store/by-id #{44 55}))
      (check-ch)) => (msg :foo.store.done/by-id #{e4 e5})
  (-> (send! in (req :foo.store/by-id #{33 55}))
      (check-ch)) => (msg :foo.store.done/by-id #{e3 e5}))


(fact
  "can retrieve all entities"

  (send! in (msg :foo.store/save #{e1 e2 e3}))
  (check-bus out)

  (-> (send! in (req :foo.store/all))
      (check-ch)) => (msg :foo.store.done/all #{e1 e2 e3}))


(fact
  "can be reset"
  (send! in (msg :foo.store/save #{e3 e4 e5}))
  (-> (send! in (req :foo.store/all))
      (check-ch)
      (payload)
      (count)) => 3
  (check-bus out)

  (send! in (msg :foo.store/reset))
  (check-bus out) => (msg :foo.store.done/reset)

  (-> (send! in (req :foo.store/all))
      (check-ch)
      (payload)) => #{})


(fact
  "can update entities"
  (send! in (msg :foo.store/update #{e2 e3}))
  (check-bus out)
  (-> (send! in (req :foo.store/all))
      (check-ch)
      (payload)) => #{e2 e3}

  (-> (send! in (req :foo.store/update {:id 33 :name "esmeralda"}))
      (check-ch)) => (msg :foo.store.done/update #{(assoc e3 :name "esmeralda")})
  (-> (send! in (req :foo.store/by-id 33))
      (check-ch)
      (payload)) => #{(assoc e3 :name "esmeralda")}

  (-> (send! in (req :foo.store/update {:id 22 :age 100}))
      (check-ch)) => (msg :foo.store.done/update #{(assoc e2 :age 100)})
  (-> (send! in (req :foo.store/by-id 22))
      (check-ch)
      (payload)) => #{(assoc e2 :age 100)})


(fact
  "can delete entities"
  (send! in (msg :foo.store/save #{e5 e6 e4 e3}))
  (check-bus out)
  (send! in (msg :foo.store/delete 55))
  (check-bus out) => (msg :foo.store.done/delete #{e5})
  (-> (send! in (req :foo.store/all))
      (check-ch)
      (payload)) => #{e3 e4 e6}

  (send! in (msg :foo.store/delete #{66 33}))
  (check-bus out) => (msg :foo.store.done/delete #{e3 e6})
  (-> (send! in (req :foo.store/all))
      (check-ch)
      (payload)) => #{e4})


(fact
  "can search entities by values"
  (send! in (msg :foo.store/save #{e1 e2 e3 e4}))
  (check-bus out)

  (send! in (msg :foo.store/by-values {:name "fofo"}))
  (check-bus out) => (msg :foo.store.done/by-values #{e1 e3})

  (send! in (msg :foo.store/by-values {:name "bubu"
                                       :age 12}))
  (check-bus out) => (msg :foo.store.done/by-values #{e2})

  (send! in (msg :foo.store/by-values {:name "lululu"}))
  (check-bus out) => (msg :foo.store.done/by-values #{}))
