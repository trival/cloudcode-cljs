(ns cloudcode.utils.data.validations-test
  (:require [midje.sweet :refer :all]
            [cloudcode.utils.data.validations :refer :all]))


(tabular
  (facts
    "about path-string?"
    (path-string? ?str) => ?res)
    ?str                ?res
    "foo.bar"           truthy
    "foo.bar.lala"      truthy
    "foo.bar.foo_bar"   truthy
    "foo"               truthy
    "foo12.lala"        truthy
    "_foo12._lala"      truthy
    "foo.12"            falsey
    "foo.bar asd"       falsey
    "3foo.bar"          falsey
    ""                  falsey
    "foo+bar"           falsey)


(tabular
  (facts
    "about symbol-name?"
    (symbol-name? ?str) => ?res)
    ?str        ?res
    "foo"       truthy
    "_foo"      truthy
    "foo_bar"   truthy
    "_foo12"    truthy
    "foo.bar"   falsey
    "foo asd"   falsey
    "3foo"      falsey
    "foo+bar"   falsey)


(tabular
  (facts
    "about email?"
    (email? ?str) => ?res)
    ?str                  ?res
    "test@foo.de"         truthy
    "test.foo@test.de"    truthy
    "test-foo@test.com"   truthy
    "test-@test.net"      truthy
    "foo.bar"             falsey
    "foo asd@lala.ru"     falsey
    "3foo"                falsey
    "test@.kp"            falsey
    "@test.kp"            falsey
    "test@foo.k"          falsey)
