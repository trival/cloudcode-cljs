(ns cloudcode.utils.data.model-test
  (:require [midje.sweet :refer :all]
            [cloudcode.utils.data.model :refer :all]))


(facts
  "about validation"

  (let [validation1 (fn [x] true)
        validation2 (fn [x] true)
        spec {:name "model1"
              :properties {:prop1 {:type :string
                                   :required? true
                                   :validations {:validation1 validation1
                                                 :validation2 validation2}}
                           :prop2 {:type :number
                                   :validations {:even even?}}}}
        nested-spec {:name "model2"
                     :properties
                     {:prop-nested {:type spec}
                      :prop-nested-set {:type :set
                                        :item-type spec}}}
        boolean? #(or (= true %) (= false %))
        validation-result? (just [boolean?
                                  (has every? keyword?)])]


    (facts
      "about type-validation"

      (let [pspec {:options #{:option1 :option2}
                   :item-type :string}]

        (tabular
          (fact
            "returns a function"
            (type-validation ?type pspec) => fn?)
            ?type
            :string
            :long
            :keyword
            :option
            :set
            :number
            :lala)

        (tabular
          (fact
            "returns constantly true for all nonsens types"
            ((type-validation ?type pspec) ?val) => true)
            ?type    ?val
            nil      nil
            :lala    nil
            :lala    true
            :lala    1000
            :lala    "lalala"
            [:lala]  nil
            [:lala]  true
            [:lala]  1000
            [:lala]  false
            #{:lala} "lalala")

        (tabular
          (fact
            "gives reasonable type checkers"
            ((type-validation ?type pspec) ?val) => ?res)
            ?type      ?val                          ?res
            :number    0.7                           truthy
            :number    "a string"                    falsey
            :string    "a string"                    truthy
            :string    true                          falsey
            :keyword   :lala                         truthy
            :keyword   {}                            falsey
            :set       #{"a string" "more string"}   truthy
            :set       #{"a string" :not-string}     falsey
            :option    :lala                         falsey
            :option    :option1                      truthy
            :option    :option2                      truthy)

        (fact
          "calls valid? with the type as spec if type is a map"
          ((type-validation {:some "spec"} pspec) ..some-value..) => "valid? result"
            (provided
              (valid? {:some "spec"} ..some-value..) => "valid? result"))))


    (facts
      "about validate-prop"

        (tabular
          (fact
            "returns a vector of validation result and validation keys pairs"
            (validate-prop spec ?prop ?val) => (has every? validation-result?))
            ?prop     ?val
            :prop1    "lala"
            :prop1    nil
            :prop1    9)

        (tabular
          (fact
            "has a result for each validation + required? + type-validation"
            (count (validate-prop spec ?prop ?val)) => ?count)
            ?prop     ?val      ?count
            :prop1    "lala"    4
            :prop2    3         2)

        (fact
          "checks all validations"
          (map second (validate-prop spec :prop1 "")) => (contains [:prop1 :validation1]
                                                                   [:prop1 :validation2]
                                                                   [:prop1 :required?]
                                                                   [:prop1 :type]
                                                                   :in-any-order)

          (map second (validate-prop spec :prop2 3)) => (contains [:prop2 :even]
                                                                  [:prop2 :type]
                                                                  :in-any-order)

          (map second (validate-prop spec :prop2 3)) =not=> (contains [:prop2 :required?])))


    (facts
      "about validate-required"

      (fact
        "produces a validation result"
        (validate-required spec {:prop1 "lala"}) => validation-result?)

      (fact
        "checks if all required props are present"
        (validate-required spec {:prop1 "lulu"}) => (just truthy vector?)
        (validate-required spec {:prop2 "lulu"}) => (just falsey vector?)))


    (facts
      "about validate-model"

      (fact
        "produces validation results"
        (validate-model spec {:prop1 "lala" :prop2 4}) => (has every? validation-result?))

      (fact
        "checks for properties and all-required"
        (validate-model spec {:prop1 "lala" :prop2 4}) => (contains (just boolean? [:all-required?]))
        (validate-model spec {:prop1 "lala" :prop2 4}) => (contains (just boolean?
                                                                          (just :prop1 anything)))
        (validate-model spec {:prop1 "lala" :prop2 4}) => (contains (just boolean?
                                                                          (just :prop2 anything))))

      (fact
        "validates nested models and sets"
        (validate-model nested-spec {:prop-nested {:prop1 "lala" :prop2 4}
                                     :prop-nested-set #{{:prop1 "LALA" :prop2 6}
                                                        {:prop1 "lala" :prop2 4}}})
          => (has every? validation-result?)
        (valid? nested-spec {:prop-nested {:prop1 "lala" :prop2 4}
                             :prop-nested-set #{{:prop1 "LALA" :prop2 6}
                                                {:prop1 "lala" :prop2 4}}})
          => true
        (valid? nested-spec {:prop-nested {:prop1 "lala" :prop2 4}
                             :prop-nested-set #{{:prop1 "LALA" :prop2 5}
                                                {:prop1 "lala" :prop2 4}}})
          => false))))


(let [spec {:properties {:prop1 {:type :any}
                         :prop2 {:type :any}}}
      nested-spec {:properties
                   {:prop-nested {:type spec}
                    :prop-nested-set {:type :set
                                      :item-type spec}}}]

  (facts
    "about purify"

    (fact
      "strips all non-specified properties from the model"
      (purify spec {:prop1 "foo" :prop-other "laalaa"})
      => {:prop1 "foo"}
      (purify nested-spec {:prop-nested {:prop1 "foo" :prop-foo "loo"}
                           :prop-nested-set
                           #{{:laa :loo}
                             {:prop2 12
                              :booo 301}}})
      => {:prop-nested {:prop1 "foo"}
          :prop-nested-set #{{:prop2 12}}}))


  (fact
    "serialize-basic purifies and strips non serializable properties"
    (let [spec {:properties {:foo1 {:type :any}
                             :foo2 {:type :any
                                    :serialize :skip}
                             :foo3 {:type :any
                                    :serialize :custom}}}]
      (serialize-basic spec {:foo1 "bla" :foo2 "luu" :foo3 "bar" :foo4 "kuku"})
      => {:foo1 "bla"})))


(facts
  "about default values"

  (fact
    "there are default values for special types"
    (get-default-value {:type :set}) => #{}
    (get-default-value {:type :string}) => ""
    (get-default-value {:type :long}) => 0
    (get-default-value {:type :edn}) => {})


  (fact
    "a default attribute in spec overrides the defaut value"
    (get-default-value {:type :set
                        :default ..a-default-val..}) => ..a-default-val..)

  (fact
    "default values can be functions that are being called"
    (get-default-value {:default (fn [] ..return-value..)})
    => ..return-value..))

  (fact
    "creates new uuid as default for type :uuid"
    (let [u1 (get-default-value {:type :uuid})
          u2 (get-default-value {:type :uuid})]
      (= u1 u2) => false
      (class u1) => java.util.UUID
      (class u2) => java.util.UUID))
