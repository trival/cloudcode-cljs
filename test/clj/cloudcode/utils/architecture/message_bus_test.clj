(ns cloudcode.utils.architecture.message-bus-test
  (:require
    [midje.sweet :refer :all]
    [helpers.async :refer [result-or-timeout]]
    [clojure.core.async :refer [go go-loop chan put! close! <!! <! >! alts!! timeout]]
    [cloudcode.utils.architecture.message-bus :refer :all]))


;; ===== Message =====

(facts
  "about messages"

  (fact
    "have a type and a type checker"
    (message? :some-key) => false
    (message? 42) => false
    (message? "some string") => false
    (message? {..some.. ..map..}) => false
    (message? [..some.. ..vector..]) => false
    (message? (msg ..id.. ..payload..)) => true)

  (fact
    "have an id"
    (id (msg ..id.. ..payload..)) => ..id..)

  (fact
    "are comparable"
    (= (msg ..id.. ..payload..) (msg ..id.. ..payload..)) => true)

  (fact
    "have a payload"
    (payload (msg ..id.. ..payload..)) => ..payload..)

  (fact
    "have its own type"
    (:type (msg ..id.. ..payload..)) => message-type)

  (fact
    "can be created without payload"
    (payload (msg ..id..)) => nil)


  (facts
    "about errors"

    (fact
      "have its own type"
      (:type (err ..id.. ..payload..)) => error-message-type)

    (fact
      "have a specified type"
      (error? :some-key) => false
      (error? 42) => false
      (error? "some string") => false
      (error? {..some.. ..map..}) => false
      (error? [..some.. ..vector..]) => false
      (error? (err ..id.. ..payload..)) => true)

  (fact
    "can be created without payload"
    (payload (err ..id..)) => nil)

    (fact
      "are messages"
      (message? (err ..id.. ..payload..)) => true)

    (fact
      "are comparable"
      (= (err ..id.. ..payload..) (err ..id.. ..payload..)) => true)

    (fact
      "messages are not errors"
      (error? (msg ..id.. ..payload..)) => false))


  (facts
    "about events"

    (fact
      "have its own type"
      (:type (evt ..id.. ..payload..)) => event-message-type)

    (fact
      "have a specified type"
      (event? :some-key) => false
      (event? 42) => false
      (event? "some string") => false
      (event? {..some.. ..map..}) => false
      (event? [..some.. ..vector..]) => false
      (event? (evt ..id.. ..payload..)) => true)

    (fact
      "can be created without payload"
      (payload (evt ..id..)) => nil)

    (fact
      "are comparable"
      (= (evt ..id.. ..payload..) (evt ..id.. ..payload..)) => true)

    (fact
      "are messages"
      (message? (evt ..id.. ..payload..)) => true))


  (facts
    "about requests"

    (fact
      "have its own type"
      (:type (req ..id.. ..payload..)) => request-message-type)

    (fact
      "have a specified type"
      (request? :some-key) => false
      (request? 42) => false
      (request? "some string") => false
      (request? {..some.. ..map..}) => false
      (request? [..some.. ..vector..]) => false
      (request? (req ..id.. ..payload..)) => true)

    (fact
      "can be created without payload"
      (payload (req ..id..)) => nil)

    (fact
      "are messages"
      (message? (req ..id.. ..payload..)) => true)

    (fact
      "messages are not requests"
      (request? (msg ..id.. ..payload..)) => false)

    (fact
      "have a response bus"
      (bus? (response-bus (req ..id.. ..payload..))) => true)))



;; ===== Service =====

(facts
  "about service"

  (fact
    "take an optional id"
    (id (service ..id..)) => ..id..)

  (fact
    "has a type checker"
    (service? :some-key) => false
    (service? 42) => false
    (service? "some string") => false
    (service? {..some.. ..map..}) => false
    (service? [..some.. ..vector..]) => false
    (service? (bus)) => false
    (service? (msg ..id.. ..payload..)) => false
    (service? (service)) => true)

  (fact
    "has its own type"
    (:type (service)) => service-type)

  (fact
    "has an input channel"
    (let [c (input-channel (service))]
      (put! c ..something..)
      (<!! c)) => ..something..)


  (facts
    "about output buses"

    (fact
      "can be registered as map and inspected"
      (let [s (service)
            out {:some-specific-output-key (bus)
                 :default (bus)}]
        (register-output! s out)
        (output s) => out))

    (fact
      "standalone default bus can be registered alone"
      (let [s (service)
            b (bus)]
        (register-output! s b)
        (output s) => {:default b})))


  (facts
    "about implementation"

    (fact
      "can be installed and retrieved"
      (let [s (service)]
        (implementation s) => nil
        (do
          (install-implementation! s ..impl..)
          (implementation s) => ..impl..)))

    (fact
      "new overrides old"
      (let [s (service)]
        (do
          (install-implementation! s ..impl1..)
          (install-implementation! s ..impl2..)
          (implementation s) => ..impl2..)))

    (fact
      "provides an interface"
      (let [s (service)
            i {..key1.. ..fn1.. ..key2.. ..fn2..}]
        (do
          (install-implementation! s i)
          (interface s)) => #{..key1.. ..key2..})))


  (facts
    "about error handler"

    (fact
      "can be installed and retrieved"
      (let [s (service)]
        (errorhandling s) => nil
        (do
          (install-errorhandling! s ..eh..)
          (errorhandling s) => ..eh..)))

    (fact
      "new overrides old"
      (let [s (service)]
        (do
          (install-errorhandling! s ..eh..)
          (install-errorhandling! s ..eh..)
          (errorhandling s) => ..eh..)))

    (fact
      "provides an interface"
      (let [s (service)
            eh {..key1.. ..fn1.. ..key2.. ..fn2..}]
        (do
          (install-errorhandling! s eh)
          (error-interface s)) => #{..key1.. ..key2..})))


  (fact
    "setup functions can be chained"
    (let [s (service)
          eh {..id.. ..hander..}
          impl {..id.. ..hander..}
          out (bus)]

      (-> s
          (install-implementation! impl)
          (install-errorhandling! eh)
          (register-output! out)
          (start-service!)
          (stop-service!)) => s))


  (facts
    "about message handling"

    (fact
      "a service handles messages for its interface ids"
      (let [s (service)
            impl {..id1.. (fn [_ _] ..handler-result1..)
                  ..id2.. (fn [_ _] ..handler-result2..)}
            m1 (msg ..id1.. ..some-data..)
            m2 (msg ..id2.. ..some-other-data..)]

        (install-implementation! s impl)

        (handle-message s m1) => ..handler-result1..
        (handle-message s m2) => ..handler-result2..))

    (fact
      "gives the message and the service output to the handler"
      (let [s (service)
            impl {..id.. (fn [in out] [in out])}
            m (msg ..id.. ..some-data..)]

        (install-implementation! s impl)
        (register-output! s (bus))

        (handle-message s m) => [m (output s)]))

    (fact
      "message processing can be started and stopped"
      (let [s (service)
            i (input-channel s)
            o (chan)
            impl {..id.. (fn [_ _] (put! o ..handler-result..))}
            m (msg ..id.. ..some-data..)]

        (install-implementation! s impl)
        (put! i m)
        (result-or-timeout o 100) => nil

        (start-service! s)

        (result-or-timeout o 100) => ..handler-result..
        (put! i m)
        (result-or-timeout o 100) => ..handler-result..

        (stop-service! s)

        (put! i m)
        (result-or-timeout o 100) => nil

        (start-service! s)

        (result-or-timeout o 100) => ..handler-result..
        (put! i m)
        (result-or-timeout o 100) => ..handler-result..

        (stop-service! s)))

    (fact
      "calling stop on service dosn't blow"
      (let [s (service)]
        (stop-service! s)))

    (fact
      "delivers input message to corresponding handler"
      (let [s (service)
            i (input-channel s)
            o (chan)
            impl {..id1.. (fn [_ _] (put! o ..handler1..))
                  ..id2.. (fn [_ _] (put! o ..handler2..))}
            m1 (msg ..id1.. ..some-data..)
            m2 (msg ..id2.. ..some-other-data..)]

        (install-implementation! s impl)
        (start-service! s)

        (put! i m1)
        (result-or-timeout o 100) => ..handler1..

        (put! i m2)
        (result-or-timeout o 100) => ..handler2..))

    (fact
      "puts error to default output if message-id cannot be handled"
      (let [s (service ..service-id..)
            out (bus)
            impl {..id1.. ..handler..}
            m (msg ..id2.. ..data..)]
        (install-implementation! s impl)
        (register-output! s out)

        (handle-message s m) => (throws)))

    (fact
      "replaces the default-out with the response-bus if handling requests"
      (let [s (service)
            r (req ..id.. ..payload..)
            default-out (bus)
            impl {..id.. (fn [_ out] (:default out))}]
        (install-implementation! s impl)
        (register-output! s default-out)

        (handle-message s r) => (response-bus r)
        (handle-message s r) =not=> default-out))

    (fact
      "errors are handled by errorhandling, not by implementations"
      (let [s (service)
            e (err ..id.. ..payload..)
            impl {..id.. (fn [in out] ..impl-handler..)}
            eh {..id.. (fn [in out] ..eh-handler..)}]
        (install-errorhandling! s eh)
        (install-implementation! s impl)

        (handle-message s e) => ..eh-handler..))

    (fact
      ":all handler handles all unspecified messages"
      (let [s (service)
            m (msg :some-id ..payload..)
            impl {:some-id (fn [in out] ..id-handler..)
                  :all (fn [in out] ..all-handler..)}]
        (install-implementation! s impl)

        (handle-message s m) => ..id-handler..
        (handle-message s (msg :other-id1 ..payload..)) => ..all-handler..
        (handle-message s (msg :other-id2 ..payload..)) => ..all-handler..))

    (fact
      ":all handler handles all unspecified messages"
      (let [s (service)
            e (err :some-id ..payload..)
            eh {:some-id (fn [in out] ..id-handler..)
                :all (fn [in out] ..all-handler..)}]
        (install-errorhandling! s eh)

        (handle-message s e) => ..id-handler..
        (handle-message s (err :other-id1 ..payload..)) => ..all-handler..
        (handle-message s (err :other-id2 ..payload..)) => ..all-handler..))))


;; ===== Bus =====

(facts
  "about message bus"

  (fact
    "have an optional id"
    (id (bus ..id..)) => ..id..)

  (fact
    "has a type checker"
    (bus? :some-key) => false
    (bus? 42) => false
    (bus? "some string") => false
    (bus? {..some.. ..map..}) => false
    (bus? [..some.. ..vector..]) => false
    (bus? (bus)) => true)

  (fact
    "has its own type"
    (:type (bus)) => bus-type)

  (fact
    "has an endpoint channel"
    (let [e (endpoint (bus))]
      (put! e ..message..)
      (<!! e)) => ..message..)

  (facts
    "about send"

    (fact
      "sends messages to endpoint"
      (let [b (bus)
            m (msg ..id.. ..payload..)]
        (send! b m)
        (result-or-timeout (endpoint b) 100) => m))

    (fact
      "sends message over default bus"
      (let [b (bus)
            m (msg ..id.. ..payload..)]
        (send! {:default b} m)
        (result-or-timeout (endpoint b) 100) => m))

    (fact
      "throws if no bus provided"
      (let [m (msg ..id.. ..payload..)]
        (send! "not-a-bus" m) => (throws #"bus")
        (send! {:default ..not-a-bus..} m) => (throws #"bus")
        (provided
          (bus? ..not-a-bus..) => false)))

    (fact
      "accept only messages"
      (let [b (bus)]
        (send! b ..not-a-message..) => (throws #"bus" #"message")
        (provided
          (message? ..not-a-message..) => false)))

    (fact
      "returns the bus endpoint when sending requests"
      (let [r (req ..id.. ..payload..)]
        (send! (bus) r) => (endpoint (response-bus r)))))


  (facts
    "about channel connection"

    (fact
      "a channel can be connected to a bus"
      (let [b (bus)
            c1 (chan)
            c2 (chan)
            m1 (msg ..id1.. ..payload..)
            m2 (msg ..id2.. ..payload..)]
        (connect! b c1)
        (connect! b c2)

        (put! c1 m1)
        (result-or-timeout (endpoint b) 100) => m1
        (put! c2 m2)
        (result-or-timeout (endpoint b) 100) => m2))

    (fact
      "a channel can be disconnected from a bus"
      (let [b (bus)
            c (chan)
            m (msg ..id.. ..payload..)]
        (connect! b c)
        (put! c m)
        (result-or-timeout (endpoint b) 100) => m

        (disconnect! b c)
        (put! c m)
        (result-or-timeout (endpoint b) 100) => nil)))


  (facts
    "about service registrations"

    (facts
      "about register-service!"

      (fact
        "bus can register services"
        (register-service! (bus) (service)) => anything)

      (fact
        "accepts only services"
        (let [b (bus)]
          (register-service! b ..not-a-service..) => (throws #"service" #"bus")
          (provided
            (service? ..not-a-service..) => false)))

      (fact
        "can register many services at once"
        (let [s1 (service)
              s2 (service)
              s3 (service)
              s4 (service)
              s5 (service)]

          (let [b (bus)]
            (register-service! b s1 s3 s5)
            (registered-services b)) => #{s1 s3 s5}

          (let [b (bus)]
            (register-service! b s1 s2 s3 s4 s5)
            (registered-services b)) => #{s1 s2 s3 s4 s5})))


    (fact
      "bus can inspect registered services"
      (let [b (bus)
            s1 (service)
            s2 (service)]

        (registered-services b) => #{}

        (do
          (register-service! b s1)
          (registered-services b)) => #{s1}

        (do
          (register-service! b s2)
          (registered-services b)) => #{s1 s2}))


    (facts
      "about remove-service!"

      (fact
        "bus can remove registered services"
        (let [b (bus)
              s1 (service)
              s2 (service)]

          (do
            (register-service! b s1 s2)
            (registered-services b)) => #{s1 s2}

          (do
            (remove-service! b s1)
            (registered-services b)) => #{s2}))

      (fact
        "removes several services at once"
        (let [b (bus)
              s1 (service)
              s2 (service)
              s3 (service)
              s4 (service)
              s5 (service)]
          (register-service! b s1 s2 s3 s4 s5)

          (do
            (remove-service! b s3 s4 s5)
            (registered-services b)) => #{s1 s2}

          (do
            (remove-service! b s1 s2)
            (registered-services b) => #{}))))

    (fact
      "has a merged interface from all registered services"
      (let [b (bus)
            s1 (service)
            s2 (service)
            impl1 {..id1.. ..handler1..
                   ..id2.. ..handler2..}
            impl2 {..id1.. ..handler3..
                   ..id3.. ..handler4..}]
        (install-implementation! s1 impl1)
        (install-implementation! s2 impl2)

        (registered-interface b) => {}

        (register-service! b s1 s2)
        (registered-interface b) => {..id1.. #{s1 s2}
                                     ..id2.. #{s1}
                                     ..id3.. #{s2}}

        (remove-service! b s1)
        (registered-interface b) => {..id1.. #{s2}
                                     ..id3.. #{s2}}))

    (fact
      "has a merged error-interface from all registered services"
      (let [b (bus)
            s1 (service)
            s2 (service)
            eh1 {..id1.. ..handler1..
                 ..id2.. ..handler2..}
            eh2 {..id1.. ..handler3..
                 ..id3.. ..handler4..}]
        (install-errorhandling! s1 eh1)
        (install-errorhandling! s2 eh2)

        (registered-errors b) => {}

        (register-service! b s1 s2)
        (registered-errors b) => {..id1.. #{s1 s2}
                                  ..id2.. #{s1}
                                  ..id3.. #{s2}}

        (remove-service! b s1)
        (registered-errors b) => {..id1.. #{s2}
                                  ..id3.. #{s2}}))

    (fact
      "error-interface and implementation interface are distict"
      (let [b (bus)
            s (service)
            impl {..id1.. ..handler1..
                  ..id2.. ..handler2..}
            eh {..id3.. ..handler3..
                ..id4.. ..handler4..}]
        (install-implementation! s impl)
        (install-errorhandling! s eh)
        (register-service! b s)

        (registered-errors b) => {..id3.. #{s}
                                  ..id4.. #{s}}
        (registered-interface b) => {..id1.. #{s}
                                     ..id2.. #{s}})))


  (fact
    "all setup function can be chained"
    (let [b (bus)
          s (service)
          c (chan)]

      (-> b
          (register-service! s)
          (connect! c)
          (remove-service! s)
          (disconnect! c)) => b))


  (facts
    "about message flow"

    (fact
      "passes matching massage ids to registered services
      instead of it's endpoint"
      (let [s (service)
            b (bus)
            m (msg ..id.. ..payload..)
            m2 (msg ..id2.. ..result..)
            out (bus)
            impl {..id.. (fn [_ out] (send! out m2))}]
        (install-implementation! s impl)
        (register-output! s out)
        (register-service! b s)
        (start-service! s)

        (send! b m)
        (result-or-timeout (endpoint b) 100) => nil
        (result-or-timeout (endpoint out) 100) => m2))

    (fact
      "errors are not forwarded to implementations"
      (let [s (service)
            b (bus)
            o (bus)
            i {..id.. (fn [in out] (send! out (msg ..id2.. in)))}
            e (err ..id.. ..payload..)]
        (install-implementation! s i)
        (register-output! s o)
        (register-service! b s)
        (start-service! s)

        (send! b e)
        (result-or-timeout (endpoint b) 100) => e
        (result-or-timeout (endpoint o) 100) => nil))

    (fact
      "errors are forwarded to errorhandling"
      (let [s (service)
            b (bus)
            o (bus)
            res (msg ..id-result.. ..something..)
            eh {..id-eh.. (fn [in out] (send! out res))}
            i {..id-impl.. (fn [in out] (send! out res))}
            e (err ..id-eh.. ..payload..)]
        (install-implementation! s i)
        (install-errorhandling! s eh)
        (register-output! s o)
        (register-service! b s)
        (start-service! s)

        (send! b e)
        (result-or-timeout (endpoint b) 100) => nil
        (result-or-timeout (endpoint o) 100) => res))

    (fact
      "messages and errors are forwarded to registered :all handlers"
      (let [s (service)
            b (bus)
            o (bus)
            res (msg ..id-result.. ..something..)
            eh {:all (fn [in out] (send! out res))}
            i {:all (fn [in out] (send! out res))}
            m (msg ..id-msg.. ..payload..)
            e (err ..id-eh.. ..payload..)]
        (install-implementation! s i)
        (install-errorhandling! s eh)
        (register-output! s o)
        (register-service! b s)
        (start-service! s)

        (send! b e)
        (result-or-timeout (endpoint o) 100) => res
        (send! b m)
        (result-or-timeout (endpoint o) 100) => res))))
