(ns helpers.async
  (:require
    [cloudcode.utils.architecture.message-bus :refer [endpoint]]
    [clojure.core.async :refer [go chan put! <!! <! >! alts!! timeout]]))


(defn result-or-timeout
  [c ms]
  (first (alts!! [c (timeout ms)])))


(def default-timeout 100)


(defn check-ch
  [ch]
  (result-or-timeout ch default-timeout))


(defn check-bus
  [bus]
  (check-ch (endpoint bus)))


(defn check-many
  [bus n]
  (loop [i 0
         rs []]
    (let [r (check-bus bus)]
      (if (and r (< i n))
        (recur (inc i) (conj rs r))
        rs))))
