(ns spec.first-test
  (:require
    [purnam.test])
  (:require-macros
    [purnam.test :refer [fact facts]]))


(facts
  "this is my first test"
  (+ 2 1) => 3
  (+ 10 11) => 21)

