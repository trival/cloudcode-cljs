module.exports = (grunt) ->

  # load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach grunt.loadNpmTasks

  grunt.initConfig

    bowerLibs: "bower_modules/"


    watch:
      styl:
        files: ['src/style/{,*/}*.styl']
        tasks: ['stylus']
      karma:
        files: ['target/karma/test.js']
        tasks: ['karma:unit:run']
      css:
        files: ['resources/public/style/*']
        tasks: []
        options:
          livereload: 9001


    karma:
      unit:
        configFile: 'karma.conf.js'
        background: true
        singleRun: false


    clean:
      dist: [
        'resources/public/script/libs.js'
        'resources/public/style/*'
      ]


    stylus:
      options:
        compress: false
      editor:
        src: 'src/style/main.styl'
        dest: 'resources/public/style/main.css'


    copy:
      assetFonts:
        expand: true
        cwd: 'bower_modules/font-awesome/fonts/'
        src: '*'
        flatten: true
        filter: 'isFile'
        dest: 'resources/public/fonts/'


    concat:
      cssLibs:
        src: [
          '<%= bowerLibs %>font-awesome/css/font-awesome.min.css'
          '<%= bowerLibs %>codemirror/lib/codemirror.css'
          '<%= bowerLibs %>codemirror/theme/monokai.css'
        ]
        dest: 'resources/public/style/libs.css'

      jsLibs:
        src: [
          '<%= bowerLibs %>react/react.js'
          '<%= bowerLibs %>codemirror/lib/codemirror.js'
          '<%= bowerLibs %>codemirror/mode/javascript/javascript.js'
          '<%= bowerLibs %>codemirror/mode/coffeescript/coffeescript.js'
          '<%= bowerLibs %>codemirror/addon/edit/closebrackets.js'
          '<%= bowerLibs %>codemirror/addon/edit/matchbrackets.js'
          '<%= bowerLibs %>codemirror/keymap/vim.js'
        ]
        dest: 'resources/public/script/libs.js'


  grunt.registerTask 'build', ['clean', 'stylus', 'concat']
  grunt.registerTask 'default', ['build', 'karma:unit:start', 'watch']
