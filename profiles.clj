{:dev

 {:clean-targets ["out" :target-path]

  :dependencies [[midje "1.6.3"]
                 [javax.servlet/servlet-api "2.5"]
                 [im.chit/purnam.test "0.5.1"]]

  :source-paths ["test/clj"]

  :plugins [[lein-pdo "0.1.1"]
            [lein-midje "3.1.3"]
            [com.cemerick/austin "0.1.5"]]

  :cljsbuild {:builds
              {:cloudcode
               {:source-paths ["src/cljs" "target/generated/cljs"]
                :compiler {:output-to "resources/public/script/dev.js"
                           :output-dir "resources/public/script/dev/"
                           :optimizations :none
                           :source-map true}}
               :karma
               {:source-paths ["test/cljs" "target/generated/cljs" "src/cljs"]
                :compiler {:output-to "target/karma/test.js"
                           :optimizations :whitespace
                           :pretty-print true}}}}

  :injections [(require 'cemerick.austin.repls)
               (defn brepl-env []
                 (reset! cemerick.austin.repls/browser-repl-env
                         (cemerick.austin/repl-env)))]

  :repl-options {:init (do (require 'cloudcode.server.main)
                           (cloudcode.server.main/run)
                           (require 'midje.repl)
                           (midje.repl/autotest))
                 :port 7777}

  :aliases {"autorepl" ["pdo" "autobuild,"
                        "repl" ":headless"]

            "autobuild" ["pdo" "cljx" "auto,"
                         "cljsbuild" "auto"]}}}
