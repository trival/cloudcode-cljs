(ns cloudcode.engine.app.interactor
  #+cljs
  (:require-macros
    [cljs.core.async.macros :refer [go]])
  (:require
    [cloudcode.engine.project.create :refer [create]]
    [cloudcode.engine.project.transforms :refer [project-output]]
    #+clj [clojure.core.async :as async :refer [go <!]]
    #+cljs [cljs.core.async :as async :refer [<!]]
    [cloudcode.utils.architecture.message-bus :refer [send! req err evt payload error?]]))


(def interactor
  {:app.action/import
   (fn [in out]
     (go (let [serializer (:serializer out)
               data (payload in)
               res (<! (send! serializer (req :project.serializer/import data)))]
           (if (error? res)
             (send! out res)
             (let [res (<! (send! serializer (req :revision.serializer/import data)))]
               (if (error? res)
                 (send! out res)
                 (send! out (evt :app.action.done/import))))))))})
