(ns cloudcode.engine.project.interactor
  #+cljs
  (:require-macros
    [cljs.core.async.macros :refer [go]])
  (:require
    [cloudcode.entities.project :refer [spec]]
    [cloudcode.engine.project.create :refer [create]]
    [cloudcode.engine.project.transforms :refer [project-output]]
    #+clj [clojure.core.async :as async :refer [go <!]]
    #+cljs [cljs.core.async :as async :refer [<!]]
    [cloudcode.utils.data.model :as m]
    [cloudcode.utils.architecture.message-bus :refer [send! req err evt payload error?]]))


(defn get-all
  [store]
  (go (payload (<! (send! store (req :project.store/all))))))


(defn return-project
  [p store]
  (go (let [revs (payload (<! (send! store (req :revision.store/by-values
                                                {:project (:id p)}))))]
        (project-output p revs))))


(defn create-handler
  [action]
  (fn [in out]
    (go (let [data (payload in)
              store (:store out)
              ps (<! (get-all store))
              [e p r] (action data ps)]
          (if e
            (send! out (err :project.action/create
                            {:error e
                             :origin in}))
            (do
              (send! store (req :revision.store/save r))
              (send! store (req :project.store/save p))
              (send! out (evt :project.action.done/create
                              (<! (return-project p store))))))))))


(defn list-handler
  [in out]
  (go (let [store (:store out)
            all (<! (get-all store))
            ps (->> all
                    (map #(return-project % store))
                    (async/merge)
                    (async/take (count all))
                    (async/reduce conj {}))]
        (send! out
               (evt :project.action.done/list (<! ps))))))


(defn update-handler
  [in out]
  (go (let [data (payload in)
            store (:store out)]
        (if (m/valid? spec data)
          (let [res (<! (send! store (req :project.store/update data)))]
            (if (error? res)
              (send! out res)
              (send! out (evt :project.action.done/update
                              (first (payload res))))))
          (send! out (err :project.action/update
                          {:error "data invalid"
                           :origin in}))))))


(def interactor
  {:project.action/create (create-handler create)
   :project.action/list list-handler
   :project.action/update update-handler})
