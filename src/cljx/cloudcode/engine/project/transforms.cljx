(ns cloudcode.engine.project.transforms)


(defn rev-map
  [revs]
  (->> revs
       (map (fn [r]
              [(:id r) r]))
       (into {})))


(defn project-output
  [p revs]
  (let [rmap (rev-map revs)]
    [(:name p)
     {:project p
      :active-revision (get rmap (:active-revision p))
      :revisions (->> revs
                      (map (fn [r]
                             [(:rev r) r]))
                      (into (sorted-map)))}]))
