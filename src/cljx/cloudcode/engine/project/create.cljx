(ns cloudcode.engine.project.create
  (:require
    [cloudcode.entities.project :as project]
    [cloudcode.entities.revision :as revision]
    [cloudcode.utils.data.model :as m]))


(defn validate-name
  [data ps]
  (let [names (->> ps (map :name) (set))
        name (:name data)]
    (when-not (and (m/valid-prop? project/spec data :name)
                   (not (names name)))
      (str "invalid name " name))))


(defn create
  [data ps]
  (if-let [error (validate-name data ps)]
    [error]
    (let [p (m/create-of project/spec data)
          r (m/create-of revision/spec
                         {:project (:id p)})
          p (assoc p :active-revision (:id r))]
      [nil p r])))
