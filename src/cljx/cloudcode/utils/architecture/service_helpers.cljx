(ns cloudcode.utils.architecture.service-helpers
  (:require
    [cloudcode.utils.architecture.message-bus :refer [send! req err evt payload error?]]))


(defn redirect
  ([new-key msg-fn]
   (fn [in out]
     (send! out (msg-fn new-key (payload in)))))
  ([new-key]
   (redirect new-key evt)))
