(ns cloudcode.utils.architecture.message-bus
  #+cljs
  (:require-macros
    [cljs.core.async.macros :refer [go go-loop alt!]])
  (:require
    #+clj [clojure.core.async :as async :refer [go go-loop chan close! <! >! put! alt!]]
    #+cljs [cljs.core.async :as async :refer [chan close! <! >! put!]]
    [clojure.set :refer [union]]))


;; ===== Type definitions =====

(def service-type ::service-type)

(def bus-type ::bus-type)

(def message-type ::message-type)

(def event-message-type ::event-message-type)
(derive event-message-type message-type)

(def request-message-type ::request-message-type)
(derive request-message-type message-type)

(def error-message-type ::error-message-type)
(derive error-message-type message-type)


(defn type-error
  [err-msg]
  #+clj (throw (RuntimeException. err-msg))
  #+cljs (throw (js/TypeError. err-msg)))


(defn message?
  [m]
  (isa? (:type m) message-type))


(defn event?
  [m]
  (isa? (:type m) event-message-type))


(defn request?
  [m]
  (isa? (:type m) request-message-type))


(defn error?
  [m]
  (isa? (:type m) error-message-type))


(defn service?
  [s]
  (isa? (:type s) service-type))


(defn bus?
  [b]
  (isa? (:type b) bus-type))


;; ===== General methods =====

(defn id
  [obj]
  (:id obj))


;; ===== Message =====

(defn msg
  ([id] (msg id nil))
  ([id payload]
   {:id id
    :payload payload
    :type message-type}))


(defn payload
  [m]
  (:payload m))


(defn evt
  ([id] (evt id nil))
  ([id payload]
   (assoc
     (msg id payload)
     :type event-message-type)))


(defn err
  ([id] (err id nil))
  ([id payload]
   (assoc
     (msg id payload)
     :type error-message-type)))


(declare bus)

(defn req
  ([id] (req id nil))
  ([id payload]
   (assoc
     (msg id payload)
     :response (bus)
     :type request-message-type)))


(defn response-bus
  [req]
  (:response req))

;; ===== Bus =====

(defn bus
  ([] (bus nil))
  ([id]
   {:id id
    :state (atom {:endpoint (chan)
                  :services #{}
                  :interface {}
                  :errors {}
                  :connections {}})
    :type bus-type}))


(defn endpoint
  [bus]
  (-> bus :state deref :endpoint))


(defn registered-services
  [bus]
  (-> bus :state deref :services))


(defn registered-interface
  [bus]
  (-> bus :state deref :interface))


(defn registered-errors
  [bus]
  (-> bus :state deref :errors))


(declare interface input-channel error-interface)

(defn update-delegation
  [bus state-key interface-fn]
  (-> bus :state
      (swap! assoc state-key
             (->> (registered-services bus)
                  (map (fn [s] [s (interface-fn s)]))
                  (reduce (fn [acc [s is]]
                            (reduce (fn [acc i]
                                      (if (get acc i)
                                        (update-in acc [i] conj s)
                                        (assoc acc i #{s})))
                                    acc is))
                          {})))))


(defn update-errors
  [bus]
  (update-delegation bus :errors error-interface))


(defn update-interface
  [bus]
  (update-delegation bus :interface interface))


(defn registered-services-for-message
  [bus msg]
  (let [interface-fn (if (error? msg)
                       registered-errors
                       registered-interface)
        registered (interface-fn bus)]
    (union (registered (id msg))
           (registered :all))))


(defn send!
  [bus msg]
  (let [b (if-let [b (:default bus)] b bus)]
    (if (and (message? msg) (bus? b))
      (do
        (if-let [services (registered-services-for-message b msg)]
          (doseq [s services]
            (put! (input-channel s) msg))
          (put! (endpoint b) msg))
        (when (request? msg) ;; handle request case
          (endpoint (response-bus msg))))
      (type-error "send! works only on a bus and a message"))))


(defn connect!
  [bus channel]
  (when-not (-> bus :state deref :connections (get channel))
    (let [stop (chan)]
      (go (loop []
            (alt!
              stop :stopped
              channel ([v]
                       (send! bus v)
                       (recur)))))
      (swap! (:state bus) update-in [:connections] assoc channel stop)))
  bus)


(defn disconnect!
  [bus channel]
  (when-let [stop (-> bus :state deref :connections (get channel))]
    (close! stop)
    (swap! (:state bus) update-in [:connections] dissoc channel))
  bus)


(defn register-service!
  [bus & services]
  (if (every? service? services)
    (do (-> (:state bus)
            (swap! update-in [:services]
                   #(set (concat % services))))
        (update-interface bus)
        (update-errors bus))
    (type-error "tried to register not a service to the bus"))
  bus)


(defn remove-service!
  [bus & services]
  (do (-> (:state bus)
          (swap! update-in [:services]
                 #(set (remove (set services) %))))
      (update-interface bus)
      (update-errors bus))
  bus)


;; ===== Service =====

(declare handle-message)

(defn service
  ([] (service nil))
  ([id]
   {:id id
    :state (atom {})
    :input (chan)
    :type service-type}))


(defn input-channel
  [service]
  (:input service))


(defn install-implementation!
  [service impl]
  (-> (:state service)
      (swap! assoc :impl impl))
  service)


(defn implementation
  [service]
  (-> service :state deref :impl))


(defn interface
  [service]
  (set (keys (implementation service))))


(defn install-errorhandling!
  [service impl]
  (-> (:state service)
      (swap! assoc :eh impl))
  service)


(defn errorhandling
  [service]
  (-> service :state deref :eh))


(defn error-interface
  [service]
  (set (keys (errorhandling service))))


(defn register-output!
  [service out]
  (let [o (if (bus? out)
            {:default out}
            out)]
    (-> (:state service)
        (swap! assoc :out o)))
  service)


(defn output
  [service]
  (-> service :state deref :out))


(defn handle-message
  [s m]
  (let [impl (if (error? m)
               (errorhandling s)
               (implementation s))
        msg-id (id m)
        out (output s)
        out (if (request? m) ;; special handling of requests
              (assoc out :default (response-bus m))
              out)]
    (if-let [handler (or (get impl msg-id)
                         (get impl :all))]
      (handler m out)
      (type-error "message could not be handled"))))


(defn start-service!
  [s]
  (when-not (-> s :state deref :stop)
    (let [stop (chan)
          in (input-channel s)]
      (go (loop []
            (alt!
              stop :stopped
              in ([v]
                  (handle-message s v)
                  (recur)))))
      (swap! (:state s) assoc :stop stop)))
  s)


(defn stop-service!
  [s]
  (when-let [stop (-> s :state deref :stop)]
    (close! stop)
    (swap! (:state s) dissoc :stop))
  s)
