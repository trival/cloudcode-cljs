(ns cloudcode.utils.data.uuid
  #+cljs
  (:require
    [cljs-uuid-utils]))

#+clj (defn uuid [] (java.util.UUID/randomUUID))

#+cljs (defn uuid [] (cljs-uuid-utils/make-random-uuid))
