(ns cloudcode.utils.data.model
"Basic functions to create and operate on data models from a data specification.
The specification is a map containing following key-value paires:

:name - (required) a symbol name that identifies the type of the model.
        Used to generate database schemas.

:properties - (required) contains a map with all the properties of the model.

:doc - a docstring describing the model.

The keys of the :properties map represent the keys of the model.
The values of the :properties map are specifications of the model property.
The prop spec map contains:

:type - (required) on of :string, :long, :option, :set, and any datomic attribute type.

:required? - specifies if the prop must be present in the model.

:identifier? - specifies the property as the unique identifier of this model.
               used for references between models.

:default - a default value for the prop, that is used if the value is not present or invalid.

:validations - a voctor of predicates that are executed against the property.
               The value of the prop is only valid if all predicates return true.

:doc - a docstring for this prop.

:db - a map with database specific configuration.

:item-type - the type of the content of a collection. can be on of the :type values or another model spec.

:options - the set of values that can be chosen as the value of the property, if :type is :option."

  (:require
    [cloudcode.utils.data.uuid :refer [uuid]]
    [clojure.set :as set]))

;; ===== Common operation on the spec =====

(def require-keys
  #{:required? :identifier?})


(defn prop-required?
  [prop-spec]
  (some true? (vals (select-keys prop-spec require-keys))))


(def required-props
  (memoize
    (fn [spec]
      (->> (:properties spec)
           (filter (fn [[k v]] (prop-required? v)))
           (map key)
           (set)))))


(def serializable-props
  (memoize
    (fn [spec]
      (->> (:properties spec)
           (remove (fn [[k v]] (#{:skip :custom} (:serialize v))))
           (map key)
           (set)))))


(def all-props
  (memoize
    (fn [spec]
      (->> (:properties spec)
           (keys)
           (set)))))


(def identifier
  (memoize
    (fn [spec]
      (->> (:properties spec)
           (filter (fn [[k v]] (:identifier? v)))
           (keys)
           (first)))))


;; ===== Model manipulation =====

(defn purify
  "strips all non-specified properties from the model.
  Deep purification using :item-type of collections."
  [spec model]
  (let [props (:properties spec)
        prop-keys (-> props keys set)]
    (->> model
         (filter (fn [[k _]] (prop-keys k)))
         (map (fn [[k v]]
                (let [type (-> props k :type)
                      sub-spec (or (-> props k :item-type)
                                   type)]
                  (if (map? sub-spec)
                    (if (set? v)
                      [k (->> v
                             (map (partial purify sub-spec))
                             (remove empty?)
                             (set))]
                      [k (purify sub-spec v)])
                    [k v]))))
         (into {}))))


(defn required-only
  "returns a version of the provided model with only the required properties"
  [spec model]
  (->> model
       (filter (fn [[k _]] ((required-props spec) k)))
       (into {})))


(defn serialize-basic
  [spec model]
  (->> (purify spec model)
       (filter (fn [[k _]] ((serializable-props spec) k)))
       (into {})))


;; ===== Instance Creation =====

(def default-values
  {:string ""
   :set #{}
   :edn {}
   :long 0
   :uuid uuid})


(defn get-default-value
  [prop-spec]
  (let [default (get prop-spec :default
                     (default-values (:type prop-spec)))]
    (if (fn? default)
      (default)
      default)))


(defn create-with
  [spec include-key?]
  (->> (:properties spec)
       (filter (fn [[k prop-spec]]
                 (or (prop-required? prop-spec) (include-key? k))))
       (map (fn [[k prop-spec]]
              [k (get-default-value prop-spec)]))
       (into {})))


(defn create
  ([spec]
   (create spec false))
  ([spec all?]
   (create-with spec (fn [_] all?))))


(defn add-props
  [spec model props]
  (merge (create-with spec props) model))


(defn create-of
  [spec obj]
  (merge (create spec) (purify spec obj)))


 ;; ===== Validation =====

(declare valid?)


(defn all?
  [bool-coll]
  (and (not (empty? bool-coll))
       (every? identity bool-coll)))


(defn type-validation
  [type pspec]
  (cond

    (keyword? type)
    (condp = type
      :string string?
      :long number?
      :float number?
      :double number?
      :number number?
      :keyword keyword?
      :option #((:options pspec) %)
      :set (fn [v] (all? (map (type-validation (:item-type pspec) pspec) v)))
      (constantly true))

    (map? type)
    (partial valid? type)

    :else (constantly true)))


(defn wrap-validation
  [f]
  (fn [v]
    (or (nil? v)
        (try
          (f v)
          (catch #+clj Exception #+cljs js/Object e
            false)))))


(defn valid-result
  [v pred ks]
  [(pred v) ks])


(defn validate-prop
  [spec k v]
  (let [pspec (-> spec :properties k)
        type-pred (type-validation (:type pspec) pspec)]

    ;; check defined validations
    (concat (->> (:validations pspec)
                 (map (fn [[v-label v-pred]]
                        (let [pred (wrap-validation v-pred)]
                          (valid-result v pred [k v-label]))))
                 (vec))

          ;; check type
          [(valid-result v type-pred [k :type])]

          ;; check required?
          (when (prop-required? pspec)
            [(valid-result v (comp not nil?) [k :required?])]))))


(defn validate-required
  [spec model]
  [(set/subset? (required-props spec)
                 (set (keys model)))
   [:all-required?]])


(defn validate-model
  [spec model]
  (when (map? model)
    (conj (->> model
               (mapcat (fn [[k v]] (validate-prop spec k v))))
          (validate-required spec model))))


(defn valid-results?
  [results]
  (->> results
       (map first)
       (all?)))


(defn false-validations
  [results]
  (->> results
       (remove first)
       (map second)))


(defn valid?
  [spec model]
  (valid-results? (validate-model spec model)))


(defn valid-prop?
  [spec model k]
  (valid-results? (validate-prop spec k (get model k))))
