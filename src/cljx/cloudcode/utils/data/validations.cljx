(ns cloudcode.utils.data.validations
  (:require [clojure.string :as string]))


(defn path-string?
  [s]
  (re-matches #"(?i)(?:[a-z_][a-z0-9_]*\.?)+" s))


(defn symbol-name?
  [s]
  (re-matches #"(?i)[a-z_][a-z0-9_]*" s))


(def nempty? (comp not empty?))


(defn email?
  [s]
  (re-matches #"(?i)[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,5}" s))
