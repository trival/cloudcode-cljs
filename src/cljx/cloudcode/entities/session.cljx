(ns cloudcode.entities.session)


(def spec
  {:name "session"
   :doc "a editor session for a project"
   :properties

   {:id
      {:type :uuid
       :identifier? true
       :doc "id of the session"}

    :user
      {:type :ref
       :references "user"
       :required? true
       :doc "the user this session belongs to"}

    :project
      {:type :ref
       :references "project"
       :required? true
       :doc "the project this session belongs to"}

    :open-units
      {:type :edn
       :doc "all opened units and their positions"}

    :layout
      {:type :edn
       :doc "layout columns and open panels"}}})
