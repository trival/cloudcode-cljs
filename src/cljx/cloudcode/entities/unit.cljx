(ns cloudcode.entities.unit
  (:require
    [cloudcode.utils.data.validations :as v]
    [cloudcode.entities.project :as project]))


(def source-language-options
  (concat project/language-options
          #{"JSON" "GLSL" "HTML" "Jade" "CSS" "Less" "Sass" "Stylus" "Text"}))


(def deps-spec
  {:name "unit-dep"
   :doc "single dependency of a code item"
   :properties

   {:path {:type :string
           :required? true
           :doc "the dependency path"}

    :alias {:type :string
            :validations {:symbol-name v/symbol-name?}
            :doc "the alias of the dependency inside the code"}}})


(def spec
  {:name "unit"
   :doc "a code unit with all its code and metadata"
   :properties

   {:id
      {:type :uuid
       :identifier? true
       :serialize :skip
       :doc "id of the unit"}

    :path
      {:type :string
       :required? true
       :validations {:path-string v/path-string?}
       :doc "the path of this namespace"}

    :type
      {:type :option
       :options #{"Code" "JSON" "String" "Macro"}
       :default "Code"
       :doc "type of the source"}

    :dependencies
      {:type :set
       :item-type deps-spec
       :store {:datomic {:component? true}}
       :doc "the code dependencies"}

    :docs
      {:type :string
       :render {:editable? true
                :render-order 1
                :when-render :not-empty}
       :doc "the docstring, prepended to source"}

    :source
      {:type :string
       :required? true
       :render {:editable? true
                :render-order 2
                :when-render :always}
       :doc "the editable code"}

    :source-language
      {:type :option
       :options source-language-options
       :default project/default-language
       :doc "language of the code, hint for editor highlighting and compiler"}

    :source-compiled
      {:type :string
       :doc "compiled source code, not editable"}

    :test
      {:type :string
       :render {:editable? true
                :render-order 3
                :when-render :on-demand}
       :doc "unit test"}

    :test-language
      {:type :option
       :options project/language-options
       :default project/default-language
       :doc "language of the test, hint for editor highlighting and compiler"}

    :test-compiled
      {:type :string
       :doc "compiled test code, not editable"}

    :on-update
      {:type :string
       :render {:editable? true
                :render-order 4
                :when-render :on-demand}
       :doc "code to be executed after injection"}

    :on-update-language
      {:type :option
       :options project/language-options
       :default project/default-language
       :doc "language of the update callback, hint for editor highlighting and compiler"}

    :on-update-compiled
      {:type :string
       :doc "compiled on-update code, not editable"}}})


(def props (:properties spec))


(def editables
  (->> props
       (filter (fn [[_ v]] (get-in v [:render :editable?])))
       (map key)
       (into #{})))


(def editor-render-order
  (->> editables
       (map (fn [k] [k (get-in props [k :render :render-order])]))
       (into {})))


(defn editor-visibility?
  [[k v]]
  (condp = (get-in props [k :render :when-render])
    :always true
    :on-demand false
    :not-empty (if (not-empty v) true false)))


(defn options
  [prop]
  (get-in props [prop :options]))
