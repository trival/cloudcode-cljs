(ns cloudcode.entities.revision
  (:require
    [cloudcode.utils.data.validations :as v]))


(def revision-default-label  "__active__")


(def interface-spec
  {:name "revision-interface"
   :doc "a mapping between a public dependency id and a concrete unit id,
        for decoupling hard dependencies and easier change management"
   :properties

   {:public-path {:type :string
                  :required? true
                  :doc "the public path for the unit, including project name
                       consistent between different revisions"}

    :unit-id {:type :uuid
              :required? true
              :doc "the actual unit id behind the public id"}}})


(def spec
  {:name "revision"
   :doc "a freese point of the project"
   :properties

   {:id
      {:type :uuid
      :identifier? true
      :serialize :skip
      :doc "the id of this project version"}

    :project
      {:type :ref
       :required? true
       :serialize :custom
       :references "project"
       :doc "references the project that this code-item belongs to"}

    :label
      {:type :string
       :required? true
       :default revision-default-label
       :validations {:not-empty not-empty}
       :doc "a custom unique version label"}

    :description
      {:type :string
       :doc "description of this revision"}

    :interface
      {:type :set
       :item-type interface-spec
       :serialize :custom
       :doc "exposes public interface ids of all units for dependency resolution"}

    :rev
      {:type :long
       :required? true
       :doc "incremental revision number, to determine history"}

    :time
      {:type :instant
       :doc "optional timestamp of this revision"}

    :root-path
      {:type :string
       :doc "the root path from which all the dependencies of the project will be calculated and assambled"}}})
