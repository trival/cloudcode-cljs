(ns cloudcode.entities.user
  (:require [cloudcode.utils.data.validations :as v]))

(def spec
  {:name "user"
   :doc "a cloudcode user"

   :properties

   {:id
      {:type :uuid
       :identifier? true
       :doc "id of the user"}

    :uid
      {:type :string
       :required? true
       :validations {:not-empty not-empty
                     :user-name v/symbol-name?}
       :store {:unique :value}
       :doc "the user identifier, single word username"}

    :email
      {:type :string
       :validations {:not-empty not-empty
                     :valid-email v/email?}
       :store {:unique :value}
       :doc "email of the user, also unique and alternative authentication"}

    :password
      {:type :string
       :validations {:min-length #(>= (count %) 6)}
       :store {:skip true}
       :doc "the user password, not stored"}

    :passhash
      {:type :string
       :validations {:not-empty not-empty}
       :doc "a hash of the user password"}}})
