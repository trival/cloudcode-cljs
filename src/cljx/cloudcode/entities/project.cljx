(ns cloudcode.entities.project
  (:require
    [cloudcode.utils.data.validations :as v]))


(def language-options
  #{"JavaScript" "CoffeeScript"})


(def default-language "JavaScript")


(def deps-spec
  {:name "public-lib"
   :doc "a project dependency from public repository"
   :properties

   {:project-name
      {:type :string
       :required? true
       :doc "the name of the required project"}

    :revision-label
      {:type :string
       :required? true
       :doc "version id of the required project"}}})


(def spec
  {:name "project"
   :doc "a editable project unit"
   :properties

   {:id
      {:type :uuid
       :identifier? true
       :serialize :skip
       :doc "id of the project"}

    :name
      {:type :string
       :required? true
       :validations {:not-empty not-empty}
       :doc "the Name of the project displayed in showcases"}

    :users
      {:type :set
       :item-type :ref
       :references "user"
       :serialize :skip
       :validations {:not-empty not-empty}
       :doc "users to which this project belongs."}

    :description
      {:type :string
       :doc "a description text of the project"}

    :licence
      {:type :string
       :doc "licence string, that will be prepended in all exported code"}

    :active-revision
      {:type :ref
       :references "revision"
       :serialize :skip
       :doc "the active revision that is beeing edited"}

    :main-lang
      {:type :option
       :required? true
       :options language-options
       :default default-language
       :doc "the default language highlighting for code
            (also in unit test and update callbacks)."}

    :user-deps
      {:type :set
       :item-type deps-spec
       :store {:datomic {:component? true}}
       :doc "dependencies used from the private user workspace"}

    :public-deps
      {:type :set
       :item-type deps-spec
       :store {:datomic {:component? true}}
       :doc "names and version of used projects from a public repository"}}})
