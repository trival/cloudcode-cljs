(ns cloudcode.test-data.editor)


(def test-tree
  {"math"
   {:label "math"
    :path "math"
    :children

      {"noise"
       {:label "noise"
        :path "math.noise"
        :children {}}

       "clamp"
       {:label "clamp"
        :path "math.clamp"}}}

   "utils"
   {:label "utils"
    :path "utils"}})


(def test-data
  {:projects
   #{{:name "test"
      :description "lala dummy project."
      :versions #{{:label "0.0.1"
                   :time 100}}
      :main-lang "JavaScript"
      :main-path "math.lerp"
      :code #{{:source "function(min, max, value) { \n\treturn Math.min(max, Math.max(min, value)); \n}"
               :path "math.clamp"
               :docs ""
               :test "lala"}

              {:source "function(min, max, value) { return clamp(min, max, value) + 10; }"
               :path "math.lerp"
               :docs "linear interpolation"
               :dependencies #{{:path "math.clamp"
                                :alias "clamp"}
                               {:path "utils.clamp"
                                :alias "clamp2"}}}

              {:source "function(min, max, value) { return Math.min(max, Math.max(min, value)) }"
               :path "utils.clamp"}}}}})
