(ns cloudcode.stores.protocol)


(defprotocol IStore
  (by-id [store ids])
  (by-values [store value-map])
  (save [store entities])
  (update [store entities])
  (delete [store ids])
  (all [store])
  (reset [store]))
