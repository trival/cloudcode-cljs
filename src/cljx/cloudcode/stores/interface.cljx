(ns cloudcode.stores.interface
  (:require
    [cloudcode.utils.architecture.message-bus :refer [send! msg evt err payload]]
    [cloudcode.stores.protocol :refer [by-id by-values save update delete all reset]]))


;; ===== Interface =====

(def default-keys
  #{:by-id
    :by-values
    :save
    :update
    :delete
    :all
    :reset})


(def store-ns-suffix ".store")


(def store-ns-done-suffix ".store.done")


;; ===== Helper functions =====

(defn store-key
  [spec key]
  (keyword (str (:name spec) store-ns-suffix) (name key)))


(defn store-done-key
  [spec key]
  (keyword (str (:name spec) store-ns-done-suffix) (name key)))


(defn store-keys
  [spec]
  (->> default-keys
       (map (fn [k] [k (store-key spec k)]))
       (into {})))


(defn store-done-keys
  [spec]
  (->> default-keys
       (map (fn [k] [k (store-done-key spec k)]))
       (into {})))


(defn wrap-single
  [x]
  (if (set? x) x #{x}))


;; ===== implementation =====

(defn create-implementation
  [store]

  (let [done-keys (store-done-keys (:spec store))
        impl-keys (store-keys (:spec store))

        by-id-handler (fn [in out]
                        (let [ids (wrap-single (payload in))]
                          (send! out (msg (:by-id done-keys)
                                          (by-id store ids)))))

        by-values-handler (fn [in out]
                          (send! out (msg (:by-values done-keys)
                                          (by-values store (payload in)))))

        save-handler (fn [in out]
                       (let [es (wrap-single (payload in))]
                         (save store es)
                         (send! out (msg (:save done-keys) es))))

        update-handler (fn [in out]
                         (let [es (wrap-single (payload in))
                               ids (map #(get % (:e-id store)) es)]
                           (update store es)
                           (send! out (msg (:update done-keys)
                                           (by-id store ids)))))

        delete-handler (fn [in out]
                         (let [ids (wrap-single (payload in))
                               es (by-id store ids)]
                           (delete store ids)
                           (send! out (msg (:delete done-keys) es))))

        all-handler (fn [in out]
                      (send! out (msg (:all done-keys) (all store))))

        reset-handler (fn [in out]
                        (reset store)
                        (send! out (msg (:reset done-keys))))]

    {(:by-id impl-keys) by-id-handler
     (:by-values impl-keys) by-values-handler
     (:save impl-keys) save-handler
     (:update impl-keys) update-handler
     (:delete impl-keys) delete-handler
     (:all impl-keys) all-handler
     (:reset impl-keys) reset-handler}))
