(ns cloudcode.stores.in-memory
  (:require
    [cloudcode.stores.protocol :refer [IStore]]
    [cloudcode.utils.data.model :as m]))


(defn store-val
  [s]
  (get @(:atom s) (:name s)))


(defrecord InMemoryStore
  [spec atom name e-id]
  IStore

  (by-id
    [s ids]
    (let [sv (store-val s)]
      (->> ids
         (map (fn [id] (get sv id)))
         (set))))

  (by-values
    [s value-map]
    (->> (store-val s)
         (vals)
         (filter (fn [item]
                   (->> value-map
                        (map (fn [[k v]] (= (get item k) v)))
                        (reduce (fn [acc i] (and acc i)) true))))
         (set)))

  (save
    [s es]
    (doseq [e es]
      (swap! atom assoc-in [name (get e e-id)] e)))

  (update
    [s es]
    (doseq [e es]
      (swap! atom update-in
             [name (get e e-id)] merge e)))

  (delete
    [s ids]
    (doseq [id ids]
      (swap! atom update-in [name] dissoc id)))

  (all
    [s]
    (-> (store-val s)
        (vals)
        (set)))

  (reset
    [s]
    (swap! atom assoc name {})))


(defn create
  [spec atom]
  (InMemoryStore. spec atom (:name spec) (m/identifier spec)))

