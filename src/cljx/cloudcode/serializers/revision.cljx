(ns cloudcode.serializers.revision
  #+cljs
  (:require-macros
    [cljs.core.async.macros :refer [go]])
  (:require
    #+clj [clojure.core.async :as async :refer [go <!]]
    #+cljs [cljs.core.async :as async :refer [<!]]
    [cloudcode.utils.data.model :as m]
    [cloudcode.entities.project :as project]
    [cloudcode.entities.revision :as revision]
    [cloudcode.entities.unit :as unit]
    [cloudcode.utils.architecture.message-bus :refer [error? payload err req evt send!]]
    [clojure.set :as s]))


(defn get-project-names
  [store-bus revs]
  (->> revs
       (map :project)
       (map #(send! store-bus
                    (req :project.store/by-values
                         {:name %})))
       (async/merge)
       (async/take (count revs))
       (async/reduce (fn [acc msg]
                       (let [p (first (payload msg))]
                         (assoc acc (:name p) (:id p))))
                     {})))


(defn get-revisions
  [store-bus revs projects]
  (->> revs
       (map :label)
       (map #(send! store-bus
                    (req :revision.store/by-values
                         {:label %})))
       (async/merge)
       (async/take (count revs))
       (async/reduce (fn [acc msg]
                       (let [r (first (payload msg))
                             p (get (->> projects
                                         (map (fn [[name id]] [id name]))
                                         (into {}))
                                    (:project r))
                             rev (->> revs
                                      (filter #(and (= (:project %) p)
                                                    (= (:label r)
                                                       (:label %))))
                                      (first))]
                         (if rev
                           (conj acc rev)
                           acc)))
                     #{})))


(defn set-project
  [projs rev]
  (let [p-name (:project rev)]
    (if-let [id (get projs p-name)]
      (assoc rev :project id)
      (str p-name))))


(defn create-units
  [revs]
  (->> revs
       (mapcat
         (fn [r]
           (->> (:interface r)
                (map (partial m/create-of unit/spec))
                (map #(assoc % :rev (:label r))))))
       (map (fn [u]
              [(str (:rev u) (:path u)) (dissoc u :rev)]))
       (reduce conj {})))


(defn set-interface
  [units rev]
  (update-in rev [:interface]
             (fn [i]
               (->> i
                    (map (fn [u]
                           {:unit-id
                            (get-in units
                                    [(str (:label rev) (:path u)) :id])
                            :public-path (:path u)}))
                    (set)))))


(defn create-revisions
  [revs units projects]
  (->> revs
       (map (partial set-interface units))
       (map (partial m/create-of revision/spec))
       (map (partial set-project projects))
       (set)))


(defn remove-by-associated-projects
  [revs projects]
  (let [proj-ns (->> projects
                     (keys)
                     (set))]
    (->> revs
         (remove (fn [r] (proj-ns (:project r))))
         (set))))


(def revision-serializer
  {:revision.serializer/import
   (fn [in out]
     (go
       (let [store (:store out)
             revs (->> (payload in) :revisions)
             projects (<! (get-project-names store revs))
             units (create-units revs)
             revisions (create-revisions revs units projects)
             no-project (remove-by-associated-projects revs projects)
             existing (<! (get-revisions store revs projects))]
         (if (< 0 (count no-project))
           (send! out (err :revision.serializer/import
                           {:error "required project not available"
                            :revisions no-project}))
           (if (< 0 (count existing))
             (send! out (err :revision.serializer/import
                             {:error "revision already exists in store"
                              :revisions existing}))
             (do (send! store (req :revision.store/save revisions))
                 (send! store (req :unit.store/save (set (vals units))))
                 (send! out (evt :revision.serializer.done/import))))))))})
