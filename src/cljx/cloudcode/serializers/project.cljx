(ns cloudcode.serializers.project
  #+cljs
  (:require-macros
    [cljs.core.async.macros :refer [go]])
  (:require
    #+clj [clojure.core.async :as async :refer [go <!]]
    #+cljs [cljs.core.async :as async :refer [<!]]
    [cloudcode.utils.data.model :as m]
    [cloudcode.entities.project :refer [spec]]
    [cloudcode.utils.architecture.message-bus :refer [error? payload err req evt send!]]))


(def project-serializer
  {:project.serializer/import
   (fn [in out]
     (go
       (let [projects (->>
                        (payload in)
                        (:projects)
                        (map (partial m/create-of spec))
                        (set))
             existing (<! (->> projects
                               (map
                                 (fn [p]
                                   (send! (:store out)
                                          (req :project.store/by-values
                                               {:name (:name p)}))))
                               (async/merge)
                               (async/take (count projects))
                               (async/reduce
                                 (fn [result response]
                                   (let [present (payload response)]
                                     (if (empty? present)
                                       result
                                       (conj result (first present)))))
                                 [])))]
         (if (empty? existing)
           (let [res (<! (send! (:store out) (req :project.store/save projects)))]
             (if (error? res)
               (send! out res)
               (send! out (evt :project.serializer.done/import))))
           (send! out (err :project.serializer/import
                           {:origin-msg in
                            :error "projects already exist"
                            :projects existing}))))))})
