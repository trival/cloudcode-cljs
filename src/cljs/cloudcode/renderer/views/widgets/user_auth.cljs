(ns cloudcode.renderer.views.widgets.user-auth
  (:require
    [cloudcode.utils.browser.console :refer [log]]
    [om.core :as om :include-macros true]))


(defn user-auth-widget
  [user]
  [:div {:class "user-auth-widget btn-group"}
   (if user
     [:span {:class "btn"} (:uid user)]
     [:a {:class "active btn"
          :on-click #(log ":app/open-modal :panel/user-login")}
      "Login"])
   [:div {:class "btn user-icon"}
    [:i {:class "fa fa-user"}]
    (if user
      [:ul {:class "sub-menu dropdown"}
       [:li [:a {:class "active"
                 :on-click #(log ":user/request-logout")}
             "Logout"]]]
      [:ul {:class "sub-menu dropdown"}
       [:li [:a {:class "active"
                 :on-click #(log ":app/open-modal :panel/user-signup")}
             "Sign up"]]])]])


