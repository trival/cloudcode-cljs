(ns cloudcode.renderer.views.widgets.project-header
  (:require
    [cloudcode.utils.browser.console :refer [log]]
    [cloudcode.renderer.dispatcher :refer [dispatch]]
    [om.core :as om :include-macros true]))


(defn project-header-widget
  [project]
  [:div {:class "project-header-widget"}
   [:h2 {:class "project-title"} (:name project)]])

