(ns cloudcode.renderer.views.app
  (:require
    [om.core :as om :include-macros true]
    [sablono.core :as html :refer-macros [html]]
    [cloudcode.renderer.project.queries :as proj-q]
    [cloudcode.renderer.user.queries :as user-q]
    [cloudcode.renderer.views.widgets.user-auth :refer [user-auth-widget]]
    [cloudcode.renderer.views.widgets.project-header :refer [project-header-widget]]
    [cloudcode.renderer.app.data :as data]
    [cloudcode.renderer.dispatcher :refer [dispatch]]
    [cloudcode.utils.browser.console :refer [log]]
    [cloudcode.utils.browser.dom :refer [stop stop-and-do]]))


;; ===== header =====

(defn header
  [app]
  [:header {:id "app-header"}
   [:h1 {:title "CloudCode"
         :class "app-logo"
         :on-click #(dispatch :app.renderer/update-layout :layout/dashboard)}
    "CC"]
   (when-let [proj (proj-q/get-active app)]
     (project-header-widget proj))
   [:nav {:class "main-menu"}]])


;; ===== panels layout =====

(defn render-panel
  [app panel-key]
  (when-let [{:keys [view data-provider]} (get data/panels panel-key)]
    [:section {:class "panel"}
     (view (data-provider app))]))


(defn render-row
  [app row]
  [:div {:class "row"}
   (map (partial render-panel app) (:panels row))
   [:div {:class "row-resizer"}
    [:i {:class "fa fa-ellipsis-v"}]]])


(defn body
  [{:keys [layout] :as app}]
  [:main {:class "row-container"}
   (map (partial render-row app) layout)])


;; ===== modal rendering =====

(defn modal
  [{:keys [modal] :as app}]
  [:aside {:class (str "modal" (if modal " open" ""))
           :on-click #(dispatch :app.renderer/close-modal)}
   (when modal
     [:div {:class "modal-container"
            :on-click (stop)}
      [:a {:class "modal-close"
           :on-click #(dispatch :app.renderer/close-modal)}
       [:i {:class "active fa fa-times fa-lg"}]]
      (render-panel app (:panel modal))])])


;; ===== app root =====

(defn app-root
  [app]
  (html
    (-> [:div {:id "app-root"}]
        (conj (header app)
              (body app)
              (modal app))
        (vec))))
