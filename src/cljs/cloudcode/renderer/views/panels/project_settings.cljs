(ns cloudcode.renderer.views.panels.project-settings
  (:require
    [sablono.core :as html :refer-macros [html]]
    [cloudcode.utils.browser.console :refer [log]]
    [cloudcode.utils.browser.dom :refer [target-val stop-and-do]]
    [cloudcode.renderer.dispatcher :refer [dispatch]]
    [om.core :as om :include-macros true]))


(defn project-settings-panel
  [proj owner]
  (reify

    om/IInitState
    (init-state [_] {:project proj})

    om/IWillReceiveProps
    (will-receive-props [_ next-props]
      (when next-props
        (om/set-state! owner :project (.-value next-props))))

    om/IRenderState
    (render-state [_ {:keys [project]}]
      (html
        (if-not project
          [:p "select a project"]
          [:form
           [:div {:class "form-group"}
            [:label {:title "Name of the project"}
             "Project name"]
            [:input {:type "text"
                     :title "required"
                     :required true
                     :value (:name project)
                     :on-change
                     #(om/set-state! owner [:project :name] (target-val %))}]]
           [:div {:class "form-group"}
            [:label {:title "Project description"}
             "Description"]
            [:input {:type "text"
                     :title "Project description"
                     :required true
                     :value (:description project)
                     :on-change
                     #(om/set-state! owner [:project :description] (target-val %))}]]
           [:div {:class "form-group form-right"}
            [:div {:class "btn-group"}
             [:button {:type "submit"
                       :on-click
                       (stop-and-do
                         #(dispatch :project.action/update
                                    (om/get-state owner :project)))}
              "Save"]]]])))))
