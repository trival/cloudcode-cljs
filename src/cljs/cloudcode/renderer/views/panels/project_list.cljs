(ns cloudcode.renderer.views.panels.project-list
  (:require
    [sablono.core :as html :refer-macros [html]]
    [cloudcode.renderer.dispatcher :refer [dispatch]]
    [cloudcode.utils.browser.console :refer [log]]
    [cloudcode.utils.browser.dom :refer [target-val stop-and-do]]
    [om.core :as om :include-macros true]))


(defn project-list-panel
  [{:keys [projects selected]}]
  [:div {:class "project-list-panel"}
   [:header
    [:h3 "Projects list"]
    [:a {:class "btn"
         :on-click #(dispatch :app.renderer/open-modal
                              :panel/project-create)}
     "create"]
    [:a {:class "btn"
         :on-click #(dispatch :project.action/list)}
     "refresh"]]
   [:ul
    (->> projects
         (map
           (fn [[n {p :project}]]
             [:li {:class (when (= p selected) "selected")
                   :on-click #(dispatch :project.renderer.active/set @p)}
              [:header
               [:h4 n]
               [:a {:class "open-project-button"
                    :on-click (stop-and-do
                                #(dispatch :project.action/open (:id @p)))}
                [:i {:class "fa fa-sign-in"}]]]
              [:p (:description p)]])))]])
