(ns cloudcode.renderer.views.panels.user-signup
  (:require
    [cloudcode.utils.browser.console :refer [log]]
    [cloudcode.utils.browser.dom :refer [stop-and-do target-val]]
    [sablono.core :as html :refer-macros [html]]
    [om.core :as om :include-macros true]))


(defn user-signup-panel
  [errors owner]
  (reify

    om/IInitState
    (init-state [_]
      {:user {:uid ""
              :email ""
              :password ""
              :rep-password ""}})

    om/IRenderState
    (render-state [_ {:keys [user]}]
      (html
        [:form
         [:div {:class "form-group"}
          [:label {:title "required"}
           "Username"]
          [:input {:type "text"
                   :title "required"
                   :required true
                   :value (:uid user)
                   :on-change
                   #(om/set-state! owner [:user :uid] (target-val %))}]]
         [:div {:class "form-group"}
          [:label {:title "required, must be a valid email address"}
           "Email"]
          [:input {:type "email"
                   :title "required, must be a valid email address"
                   :required true
                   :value (:email user)
                   :on-change
                   #(om/set-state! owner [:user :email] (target-val %))}]]
         [:div {:class "form-group"}
          [:label {:title "required, min-length of 6 characters"}
           "Password"]
          [:input {:type "password"
                   :title "required, min-length of 6 characters"
                   :required true
                   :pattern ".{6,}"
                   :value (:password user)
                   :on-change
                   #(om/set-state! owner [:user :password] (target-val %))}]]
         [:div {:class "form-group"}
          [:label {:title "required, min-length of 6 characters"}
           "Repeat password"]
          [:input {:type "password"
                   :title "required, min-length of 6 characters"
                   :required true
                   :pattern ".{6,}"
                   :value (:rep-password user)
                   :on-change
                   #(om/set-state! owner [:user :rep-password] (target-val %))}]]
         [:div {:class "form-group form-right"}
          [:div {:class "btn-group"}
           [:button {:type "submit"
                     :on-click
                     (stop-and-do
                       #(log ":user/validate-signup (om/get-state owner :user)"))}
            "Sign up"]]]]))))

