(ns cloudcode.renderer.views.panels.project-create
  (:require
    [sablono.core :as html :refer-macros [html]]
    [cloudcode.renderer.dispatcher :refer [dispatch]]
    [cloudcode.utils.browser.console :refer [log]]
    [cloudcode.utils.browser.dom :refer [target-val stop-and-do]]
    [om.core :as om :include-macros true]))


(defn project-create-panel
  [errors owner]
  (reify

    om/IInitState
    (init-state [_]
      {:project {:name ""}})

    om/IRenderState
    (render-state [_ {:keys [project]}]
      (html
        [:form
         [:div {:class "form-group"}
          [:label {:title "required"}
           "Project name"]
          [:input {:type "text"
                   :title "required"
                   :required true
                   :focus true
                   :value (:name project)
                   :on-change
                   #(om/set-state! owner [:project :name] (target-val %))}]]
         [:div {:class "form-group form-right"}
          [:div {:class "btn-group"}
           [:button {:type "submit"
                     :on-click
                     (stop-and-do
                       #(dispatch :project.action/create
                                  (om/get-state owner :project)))}
            "Create"]]]]))))
