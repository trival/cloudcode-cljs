(ns cloudcode.renderer.state)

(def app
  (atom
    {; :public-projects #{} ; set of publicly available projects
     :projects {} ; projects by name
     :project nil
     ; :revisions #{} ; revisions needed for the project
     :revision nil
     :units {} ; map of publicpaths to units
     :unit nil

     :session nil
     :user nil
     :layout []
     :modal nil}))

