(ns cloudcode.renderer.constants)

(def cm-options-default
  {:mode "javascript"
   :theme "monokai"
   :smartIndent true
   :indentUnit 4
   :keyMap "vim"
   :lineNumbers true
   :viewportMargin js/Infinity
   :matchBrackets true
   :autoCloseBrackets true})

(def cm-options-coffeescript
  {:mode "coffeescript"
   :indentUnit 2})
