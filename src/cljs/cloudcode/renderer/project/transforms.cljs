(ns cloudcode.renderer.project.transforms)


(defn project-list
  [app]
  {:selected (:project app)
   :projects (:projects app)})


(defn selected-project
  [app]
  (:project app))
