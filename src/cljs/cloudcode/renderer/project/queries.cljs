(ns cloudcode.renderer.project.queries
  (:require
    [cloudcode.renderer.state :refer [app]]))


;; ----- getters (use everywhere) -----

(defn get-active
  [app]
  (get app :project))


;; ----- setters (use only in domain action) -----

(defn set-active!
  [state p]
  (swap! state assoc-in [:project] p))


(defn unset-active!
  [state]
  (swap! state dissoc :project))
