(ns cloudcode.renderer.project.actions
  (:require
    [cloudcode.renderer.state :refer [app]]
    [cloudcode.utils.architecture.message-bus :refer [payload]]
    [cloudcode.renderer.project.queries :as q]))


(def actions

  {:project.renderer.active/set
   (fn [in _]
     (q/set-active! app (payload in)))


  :project.renderer.active/unset
   (fn [_ _]
     (q/unset-active! app))})
