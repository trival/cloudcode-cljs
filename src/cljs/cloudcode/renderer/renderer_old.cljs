(ns cloudcode.renderer.renderer-old)
; (ns cloudcode.renderer.renderer
  ; (:require
    ; [om.core :as om :include-macros true]
    ; [clojure.string :as str]
    ; [cloudcode.renderer.state :refer [app]]
    ; [cloudcode.renderer.code-item :as item]
    ; [cloudcode.entities.unit :as model]
    ; [cloudcode.utils.data.validations :refer [path-string?]]
    ; [sablono.core :as html :refer-macros [html]]))


; ;; ----- Namespace Tree Navigation Rendering -----

; (defn tree-node-component
  ; [node owner]
  ; (reify

    ; om/IInitState
    ; (init-state [_] {:expanded? false})

    ; om/IShouldUpdate
    ; (should-update [_ _ _] true)

    ; om/IRenderState
    ; (render-state
      ; [_ {:keys [expanded?]}]

      ; (let [children (:children node)
            ; open? (and (not children)
                       ; (some #{(:path node)} (get-in @app [:editor :active-paths])))
            ; class-name (str (if children "tree-branch" "tree-leaf")
                            ; (when expanded? " expanded"))
            ; li-class-name (str "fa fa-li " (if children
                                             ; (if expanded? "fa-caret-down" "fa-caret-right")
                                             ; (if open? "fa-circle" "fa-circle-o")))
            ; on-click (if children
                       ; #(om/update-state! owner :expanded? not)
                       ; #(item/open (:path @node)))]

        ; (html [:li
               ; [:i {:class li-class-name
                    ; :on-click on-click}]
               ; [:h4 {:class class-name
                     ; :on-click on-click}
                ; (:label node)]
               ; (when (and children expanded?)
                 ; (tree-node-list-component children))])))))


; (defn tree-node-list-component
  ; [nodes]
  ; [:ul {:class "fa-ul"}
   ; (map #(om/build tree-node-component %)
        ; (vals nodes))])


; ;; ----- Path Editor Rendering -----

; (defn append-editor
  ; [owner editor]
  ; (let [node (om/get-node owner)]
    ; (set! (.-innerHTML node) "")
    ; (.appendChild node (:dom-node editor))
    ; (.scrollIntoView (:instance editor) nil)))


; (defn editor-component
  ; [{:keys [editor class-name]} owner]
  ; (reify
    ; om/IDidMount
    ; (did-mount [_]
      ; (append-editor owner editor))

    ; om/IDidUpdate
    ; (did-update [_ _ _]
      ; (append-editor owner editor))

    ; om/IRender
    ; (render [_]
      ; (html [:div {:class (str "unit-editor " class-name)} class-name]))))


; (defn unit-editors
  ; [editors path visible]
  ; (let [class-suffix (str/replace path "." "-")]
    ; (->> (sort-by model/editor-render-order (keys editors))
         ; (map (fn [k]
                ; (when (visible k)
                  ; (om/build editor-component
                            ; {:editor (editors k)
                             ; :class-name (str (name k) "-" class-suffix)})))))))


; (defn menu-color
  ; [visible? value]
  ; (if visible?
    ; "green"
    ; (if (empty? value)
      ; "gray"
      ; "blue")))


; (defn unit-toggle
  ; [data state owner]
  ; (let [ks (keys (:visible state))]
    ; [:nav
     ; {:class "unit-toggle"}
     ; (map (fn [k]
            ; [:a {:href "#"
                 ; :class (menu-color (get-in state [:visible k]) (k data))
                 ; :on-click #(om/update-state! owner [:visible k] not)}
             ; (name k)])
          ; ks)
     ; [:i {:class "unit-toggle-icon fa fa-bars"}]]))


; (defn unit-header
  ; [path]
  ; [:h4
   ; {:id (str "unit-" (str/replace path "." "-"))}
   ; [:i {:class "unit-close fa fa-times-circle-o"
        ; :on-click #(item/close path)}]
   ; path])


; (defn unit-action-buttons
  ; [path]
  ; [:div {:class "btn-group unit-actions"}
   ; [:div {:class "btn-group unit-extra-actions"}
    ; [:button {:class "btn btn-red"
              ; :type "button"
              ; :on-click #(item/delete path)}
     ; "Delete"]
    ; [:button {:class "btn"
              ; :type "button"
              ; :on-click #(.log js/console "copy as button clicked")}
     ; "Copy As"]
    ; [:button {:class "btn"
              ; :type "button"
              ; :on-click #(.log js/console "rename button clicked")}
     ; "Rename"]
    ; [:span {:class "unit-extra-actions-icon"}
     ; [:i {:class "fa fa-ellipsis-h"}]]]
   ; [:button {:class "btn"
             ; :type "button"
             ; :on-click #(.log js/console "save button clicked")}
    ; "Save"]])


; (defn unit-deps-component
  ; [data owner]
  ; (reify

    ; om/IInitState
    ; (init-state [_]
      ; (zipmap (om/value (:dependencies data)) (repeat false)))

    ; om/IRenderState
    ; (render-state [_ state]
      ; (html [:ul {:class "unit-dependencies"}
             ; (->> (:dependencies data)
                  ; (map-indexed
                    ; (fn [i dep]
                      ; [:li {:class "unit-dependency-item"}
                       ; [:input {:class "input unit-dependency-path"
                                ; :type "text"
                                ; :on-focus #(om/set-state! owner [dep] true)
                                ; :on-blur (fn [] (js/setTimeout
                                                  ; #(om/set-state! owner [dep] false)
                                                  ; 200))
                                ; :value (:path dep)}]

                       ; [:ul {:class (str "unit-dependency-selection dropdown"
                                         ; (when-not (state dep) " hidden"))}
                        ; (->> (get-in @app [:editor :available-nss])
                             ; (map (fn [n]
                                    ; [:li {:on-click
                                          ; (fn []
                                            ; (.log js/console (str "dep-select: " n))
                                            ; (om/update! data [:dependencies i :path] n)
                                            ; (om/set-state! owner [dep] false))}
                                     ; n])))]
                       ; [:span " as "]
                       ; [:input {:class "input unit-dependency-alias"
                                ; :type "text"
                                ; :on-change #(om/update! data [:dependencies i :alias]
                                                        ; (.. % -target -value))
                                ; :value (:alias dep)}]
                       ; [:i {:class "unit-dependency-delete fa fa-minus-circle"
                            ; :on-click #(om/transact! data [:dependencies]
                                                     ; (partial remove #{dep}))}]])))
             ; [:li {:class "unit-dependency-add"}
              ; [:i {:class "fa fa-plus-circle"}]]]))))


; (defn editor-visibility-state
  ; [data]
  ; (let [editors (filter (fn [[k _]] (model/editables k)) data)]
    ; (->> editors
         ; (map model/editor-visibility?)
         ; (zipmap (keys editors)))))


; (defn code-unit-component
  ; [{:keys [editors data] :as itm} owner]
  ; (reify

    ; om/IInitState
    ; (init-state [_]
      ; {:visible
       ; (into {:opts false
              ; :deps (not (empty? (:dependencies data)))}
             ; (editor-visibility-state data))})

    ; om/IRenderState
    ; (render-state [_ state]
      ; (let [path (:path data)]
        ; (html [:li {:class "unit"}
               ; (unit-header path)
               ; (unit-toggle data state owner)
               ; (when (get-in state [:visible :deps])
                 ; (om/build unit-deps-component data))
               ; (binding [om/*cursor* itm]
                 ; (unit-editors editors path (:visible state)))
               ; (unit-action-buttons path)])))))


; ; ----- Editor View -----

; (defn project-editor-component
  ; [editor]
  ; [(editor-header-component)
   ; [:section {:id "namespace-navigation"}
    ; (tree-node-list-component (:ns-tree editor))]
   ; [:section {:id "namespace-editors"}
    ; [:ul (->> (:active-paths editor)
              ; (map #(get-in editor [:loaded-nss %]))
              ; (map #(om/build code-unit-component %)))]]])


; ; ----- Projects View -----

; (defn project-settings
  ; [p]
  ; [:form
   ; [:label
    ; [:span "Name"]
    ; [:input {:type :text
             ; :value (:name p)
             ; :on-change #(om/update p :name (.. % -target -value))}]]])


; (defn project-overview-component
  ; [admin]
  ; [[:header {:id "app-header"}
    ; [:h1 "Cloudcode"]]
   ; [:section {:id "project-navigation"}
    ; [:h3 "Projects"]
    ; [:ul {:class "project-list"}
     ; (->> (:projects admin)
          ; (sort-by :name)
          ; (map (fn [p]
                 ; [:li {:on-click
                       ; #(om/update admin :active-project p)}
                  ; (:name p)])))]]
   ; [:section {:id "project-manager"
              ; :class "project-configuration"}
    ; (when-let [p (:active-project admin)]
      ; (project-settings p)
      ; [:div {:class "btn-group"}
       ; [:button {:class "btn btn-green"}
        ; "Save"]
       ; [:button {:class "btn btn-blue"}
        ; "Edit"]
       ; [:button {:class "btn btn-gray"}
        ; "Abort"]
       ; [:button {:class "btn btn-red"}
        ; "Delete"]])]])
