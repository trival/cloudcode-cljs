(ns cloudcode.renderer.dispatcher
  (:require
    [cloudcode.utils.architecture.message-bus :as arch
     :refer [send! evt]]))


(defn dispatch
  ([id] (dispatch id nil))
  ([id payload]
   (send! cloudcode.editor.infrastructure/dispatcher-bus
          (evt id payload))))


