(ns cloudcode.renderer.unit.actions
  (:require-macros
    [cljs.core.async.macros :refer [go]])
  (:require
    [cloudcode.renderer.state :refer [app]]
    [om.core :as om :include-macros true]
    [cljs.core.async :refer [chan put! <! >! close!]]))


; (defn create-editor
  ; "creates a map with a new CodeMirror instance and its DOM node"
  ; ([] (create-editor {}))
  ; ([opts]
   ; (let [opts (merge consts/cm-options-default opts)
         ; a (atom {})
         ; get-dom-node (fn [node] (swap! a assoc :dom-node node))]
     ; (try
       ; (let [editor (js/CodeMirror get-dom-node (clj->js opts))]
         ; (swap! a assoc :instance editor))
       ; (catch js/Object e
         ; (.log js/console (str "cm creation error: " e))
         ; (reset! a nil)))
     ; @a)))


; (defn update-editors
  ; [item]
  ; (->> (:data item)
       ; (filter (fn [[k v]] (model/editables k)))
       ; (reduce (fn [item [k v]]
                 ; (if (k (:editors item))
                   ; item ; do nothing if one editor already exists
                   ; (assoc-in item [:editors k] (create-editor {:value v}))))
               ; item)))


; (defn create
  ; [data]
  ; (update-editors {:data data
                   ; :editors {}}))


; (defn activate
  ; [path]
  ; (when-not (some #{path} (@app :active-paths))
    ; (swap! app update-in [:active-paths] conj path)))


; (defn load
  ; [path]
  ; (go (when-let [data (<! (remote/get-ns-data path))]
        ; (swap! app update-in [:loaded-nss] assoc path (create data))
        ; true)))


; (defn open
  ; [path]
  ; (if ((@app :loaded-nss) path)
    ; (activate path)
    ; (go (when (<! (load path))
          ; (activate path)))))


; (defn close
  ; [path]
  ; (swap! app update-in [:active-paths] #(vec (remove #{path} %))))


; (defn delete
  ; [path]
  ; (close path)
  ; (swap! app update-in [:loaded-nss] dissoc path))


; (defn copy-as
  ; [item new-name])


; (defn rename
  ; [item new-name])


(def routes {})
