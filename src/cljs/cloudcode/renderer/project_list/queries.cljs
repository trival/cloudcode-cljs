(ns cloudcode.renderer.project-list.queries)


;; ----- setters (use only in domain action) -----

(defn add!
  [state p]
  (swap! state update-in [:projects] conj p))


(defn reset!
  [state new-list]
  (swap! state assoc :projects {})
  (doseq [p new-list]
    (add! state p)))
