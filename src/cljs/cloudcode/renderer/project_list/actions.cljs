(ns cloudcode.renderer.project-list.actions
  (:require
    [cloudcode.renderer.state :refer [app]]
    [cloudcode.utils.architecture.message-bus :refer [payload]]
    [cloudcode.renderer.project-list.queries :as q]))


(def actions
  {:project.renderer.list/add
   (fn [in out]
     (q/add! app (payload in)))

  :project.renderer.list/remove
   (fn [in out])

  :project.renderer.list/reset
   (fn [in out]
     (q/reset! app (payload in)))})
