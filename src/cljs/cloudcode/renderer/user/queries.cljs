(ns cloudcode.renderer.user.queries)


;; ----- getters (use with derefereced state) -----

(defn get-user
  [app]
  (:user app))


;; ----- setters (use only in domain action) -----

(defn set-user!
  [state user]
  (swap! state assoc-in [:user] user))


(defn unset-user!
  [state]
  (set-user! state nil))

