(ns cloudcode.renderer.user.actions
  (:require-macros
    [cljs.core.async.macros :refer [go]])
  (:require
    [cloudcode.renderer.state :refer [app]]
    [cljs.core.async :refer [<!]]
    [cloudcode.utils.browser.console :refer [log]]
    [cloudcode.renderer.user.queries :as q]
    [cloudcode.entities.user :refer [spec]]
    [cloudcode.utils.data.model :as m]
    [cloudcode.utils.data.validations :as validations]))


(defn check-user
  [data]
  (when (m/valid? spec data)
    (log ":user/start-user-session data")))


(defn validate-login
  [data]
  ;; setup login user with validated properties
  (let [user (m/create-with spec #{:uid :email :password})
        user (if (validations/email? (:uid data))
               (assoc user :email (:uid data))
               (if (m/valid-prop? spec data :uid)
                 (assoc user :uid (:uid data))
                 user))
        user (if (m/valid-prop? spec data :password)
               (assoc user :password (:password data))
               user)]

    ;; check if all needed properties were set
    (if (or (and (empty? (:uid user))
                 (empty? (:email user)))
            (empty? (:password user)))
      (log ":error.user-login/invalid-input")
      (log ":user/request-login user"))))


(defn validate-signup
  [user]
  (if (and (:email user)
           (:password user)
           (m/valid? spec (m/create-of spec user))
           (= (:password user)
              (:rep-password user)))
    (log ":user/request-signup (m/purify spec user)")
    (log ":error.user-signup/invalid-input user")))


(defn process-user-response
  "check the login or signup server response
  and start a new user session if no errors"
  [success? data network-error]
  (if-not success?
    (log "network-error")
    (if-let [error (:error data)]
      (log "error")
      (if (m/valid? spec data)
        (do
          (log ":app/close-modal")
          (log ":user/start-user-session data"))
        (log ":error.network/invalid-data")))))


; (defn request-login
  ; "sending #{:email :password} or #{:uid :password}
  ; combination to the server"
  ; [user]
  ; (go (let [[success? data] (<! (r/login user))]
        ; (process-user-response
          ; success? data :error.network/user-login))))


; (defn request-signup
  ; "sending new userdata to server"
  ; [user]
  ; (go (let [[success? data] (<! (r/signup user))]
        ; (process-user-response
          ; success? data :error.network/user-signup))))


(defn start-user-session
  [user]
  ;(socket/reconnect!)
  (q/set-user! app user)
  (log :app/update-layout)
  (log :project/fetch-user-projects))


; (defn request-logout
  ; []
  ; (go (let [[success? data] (<! (r/logout))]
        ; (if (and success? (nil? (:uid data)))
          ; (log :user/logout)
          ; (log :error.network/user-logout)))))


(defn logout
  []
  ;(socket/reconnect!)
  (q/unset-user! app)
  (log :app/update-layout))


; (def routes
  ; {:user/request-login request-login
   ; :user/validate-login validate-login
   ; :user/request-signup request-signup
   ; :user/validate-signup validate-signup
   ; :user/check-user check-user
   ; :user/start-user-session start-user-session
   ; :user/request-logout request-logout
   ; :user/logout logout})
