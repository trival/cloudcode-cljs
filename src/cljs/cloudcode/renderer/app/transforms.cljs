(ns cloudcode.renderer.app.transforms)


(defn layout-key
  [user? project?]
  (if user?
    (if project?
      :layout/project
      :layout/user)
    :layout/no-user))

