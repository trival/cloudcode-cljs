(ns cloudcode.renderer.app.data
  (:require
    [cloudcode.renderer.views.panels.user-login :refer [user-login-panel]]
    [cloudcode.renderer.views.panels.user-signup :refer [user-signup-panel]]
    [cloudcode.renderer.views.panels.project-settings :refer [project-settings-panel]]
    [cloudcode.renderer.views.panels.project-create :refer [project-create-panel]]
    [cloudcode.renderer.views.panels.project-list :refer [project-list-panel]]
    [cloudcode.renderer.project.transforms :as proj-t]
    [om.core :as om :include-macros true]))


;; ----- Layouts -----

(def layouts
  {:layout/dashboard
    [{:panels
      [:panel/user-projects]}
     {:panels
      [:panel/project-settings]}]

   :layout/project
    [{:panels
      [:panel/namespace-tree]}
     {:panels
      [:panel/unit-editor]}]})


;; ----- Panels -----

(defn dummy-panel
  [label]
  {:view (fn [_]
           [:div {:class "placeholder-panel"
                  :style {:background "rgba(255,0,0,0.3)"
                          :padding "1em"}}
            label [:br] "not yet implemented"])
   :data-provider (fn [_] nil)})


(def panels
  {:panel/user-login
   {:view #(om/build user-login-panel %)
    :data-provider (fn [_] nil)}

   :panel/user-signup
   {:view #(om/build user-signup-panel %)
    :data-provider (fn [_] nil)}

   :panel/user-settings
   (dummy-panel "User Settings")

   :panel/user-projects
   {:view project-list-panel
    :data-provider proj-t/project-list}

   :panel/project-settings
   {:view #(om/build project-settings-panel %)
    :data-provider proj-t/selected-project}

   :panel/project-create
   {:view #(om/build project-create-panel %)
    :data-provider (fn [_] nil)}

   :panel/namespace-tree
   (dummy-panel "Namespace Tree")

   :panel/unit-editor
   (dummy-panel "Unit Editor")})


