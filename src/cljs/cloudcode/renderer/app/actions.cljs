(ns cloudcode.renderer.app.actions
  (:require
    [cloudcode.utils.architecture.message-bus :refer [payload]]
    [cloudcode.renderer.app.queries :as q]
    [cloudcode.renderer.app.data :as d]
    [cloudcode.renderer.state :refer [app]]
    [cloudcode.utils.browser.dom :as dom]
    [cloudcode.utils.browser.console :refer [log]]))


(def actions

  {:app.renderer/refresh-style
   (fn [_ _]
     (dom/reload-style (dom/by-id "styles")))


   :app.renderer/re-render-ui
   (fn [_ _]
     (q/set-timestamp! app))


   :app.renderer/update-layout
   (fn [in _]
     (->> (payload in)
          (get d/layouts)
          (q/set-layout! app)))


   :app.renderer/open-modal
   (fn [in _]
     (let [panel-key (payload in)]
       (q/set-modal! app {:panel panel-key})))


   :app.renderer/close-modal
   (fn [_ _]
     (q/unset-modal! app))})
