(ns cloudcode.renderer.app.queries)


;; ----- getters (use everywhere) -----


;; ----- setters (use only in domain action) -----

(defn set-timestamp!
  [state]
  (swap! state assoc :dev-timestamp (.now js/Date)))


(defn set-layout!
  [state layout]
  (swap! state assoc :layout layout))


(defn set-modal!
  [state modal]
  (swap! state assoc :modal modal))


(defn unset-modal!
  [state]
  (set-modal! state nil))
