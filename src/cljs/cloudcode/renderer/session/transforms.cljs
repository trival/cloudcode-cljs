(ns cloudcode.renderer.session.transforms
  (:require
    [clojure.string :refer [split join]]
    [cloudcode.renderer.state :refer [app]]))


(defn add-ns
  [tree ns]
  (loop [path (split ns #"\.")
         done []
         tree tree]
    (let [label (first path)
          remaining (next path)
          done (conj done label)]
      (when label
        (let [tree-path (interpose :children done)
              node (get-in tree tree-path
                           {:label label
                            :path (join \. done)})
              tree (assoc-in tree tree-path node)]
          (if remaining
            (recur remaining done tree)
            tree))))))


(defn from-nss!
  [nss]
  (swap! app assoc :ns-tree (reduce add-ns {} nss)))
