(ns cloudcode.renderer.session.actions
  (:require-macros
    [cljs.core.async.macros :refer [go]])
  (:require
    [cljs.core.async :refer [<!]]
    [cloudcode.renderer.state :refer [app]]))


; (defn update!
  ; [f & args]
  ; (apply swap! app update-in [:available-nss] f args)
  ; (tree/from-nss! (@app :available-nss)))


; (defn add-new
  ; [ns]
  ; (update! conj ns))


; (defn delete
  ; [ns]
  ; (update! disj ns)
  ; (item/delete ns))


; (defn add-many
  ; [nss]
  ; (update! into nss))


; (defn from-remote
  ; []
  ; (go (let [nss (<! (dc/get-namespaces))]
        ; (update! (fn [] nss)))))


(def routes {})

