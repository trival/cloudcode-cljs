(ns cloudcode.utils.browser.netio
  (:require
    [ajax.core :refer [ajax-request edn-request-format edn-response-format]]
    [cljs.core.async :refer [chan put! close!]]))


(defn async-handler
  [ch res]
  (put! ch res)
  (close! ch))


(defn ajax
  ([url method]
   (ajax url method nil))
  ([url method data]
   (let [out (chan)]
     (ajax-request {:uri url
                    :method method
                    :format (merge (edn-request-format) (edn-response-format))
                    :handler (partial async-handler out)
                    :params data})
     out)))
