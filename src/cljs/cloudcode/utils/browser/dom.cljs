(ns cloudcode.utils.browser.dom)


(defn by-id
  "shortcut for document.getElementById"
  [s]
  (.getElementById js/document s))


(defn reload-style
  "reloads the style of the given link element
  by appending a new timestamp to the url query"
  [style-link]
  (let [query-str (str "?reload=" (.now js/Date))
        href (.-href style-link)
        href (.replace href #"\?.*|$" query-str)]
    (set! (.-href style-link) href)))


(defn stop-and-do
  "returns a event listener that has called preventDefault
  and stopPropagation before calling the supplied function
  with the event"
  [f]
  (fn [e]
    (.preventDefault e)
    (.stopPropagation e)
    (f e)))


(defn stop
  "returns a event listener that just calls reventDefault
  and stopPropagation on the event"
  []
  (stop-and-do (fn [e] nil)))


(defn target-val
  "returns the value attribute of the target of the
  given event"
  [event]
  (.. event -target -value))
