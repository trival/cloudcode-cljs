(ns cloudcode.utils.browser.console)


(defn log
  [& args]
  (.apply (.-log js/console) js/console (apply array args)))


(defn debug
  [& args]
  (.apply (.-debug js/console) js/console (apply array args)))


(defn error
  [& args]
  (.apply (.-error js/console) js/console (apply array args)))
