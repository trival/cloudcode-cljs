(ns cloudcode.editor.mediators.app
  (:require
    [cloudcode.utils.architecture.message-bus :refer [send! req err evt payload error?]]))

(def app-mediator
  {:app.action.done/import
   (fn [in out]
     (send! (:dispatcher out) (evt :project.action/list)))})
