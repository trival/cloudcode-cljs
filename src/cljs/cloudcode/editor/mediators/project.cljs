(ns cloudcode.editor.mediators.project
  (:require
    [cloudcode.utils.browser.console :refer [log]]
    [cloudcode.utils.architecture.message-bus :refer [send! req err evt payload error?]]))


(def project-mediator

  {:project.action.done/create
   (fn [in out]
     (send! out (evt :project.renderer.list/add
                     (payload in)))
     (send! out (evt :app.renderer/close-modal)))


   :project.action.done/list
   (fn [in out]
     (send! out (evt :project.renderer.list/reset
                     (payload in))))


   :project.action.done/update
   (fn [in out]
     (let [p (payload in)]
       (log (str p))
       (send! out (evt :project.renderer.active/set p))
       (send! (:dispatcher out) (evt :project.action/list))))})
