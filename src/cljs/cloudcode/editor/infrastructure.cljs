(ns cloudcode.editor.infrastructure
  (:require
    [cloudcode.entities.unit :as unit]
    [cloudcode.entities.project :as project]
    [cloudcode.entities.revision :as revision]
    [cloudcode.entities.user :as user]
    [cloudcode.entities.session :as session]

    [cloudcode.stores.interface :refer [create-implementation]]
    [cloudcode.stores.in-memory :refer [create]]

    [cloudcode.serializers.revision :refer [revision-serializer]]
    [cloudcode.serializers.project :refer [project-serializer]]

    [cloudcode.renderer.app.actions :as app-a]
    [cloudcode.renderer.project.actions :as proj-a]
    [cloudcode.renderer.project-list.actions :as proj-list-a]

    [cloudcode.editor.mediators.app :refer [app-mediator]]
    [cloudcode.editor.mediators.project :refer [project-mediator]]

    [cloudcode.engine.project.interactor :as proj-i]
    [cloudcode.engine.app.interactor :as app-i]

    [cloudcode.utils.browser.console :refer [log error]]
    [cloudcode.utils.architecture.message-bus :as arch
     :refer [service service? bus send! connect! endpoint install-implementation! install-errorhandling! register-output! register-service! start-service!]]))


;; ===== Error handler =====

(def error-service
  (-> (service "error-service")
      (install-implementation!
        {:all (fn [in _]
                (log "unhandled message! " (str in) in))})
      (install-errorhandling!
        {:all (fn [in _]
                (error "an error occured! " (str in) in))})
      (start-service!)))


(def error-bus
  (-> (bus "error-bus")
      (register-service! error-service)))


;; ===== Dispatcher =====

(def dispatcher-bus (bus "dispatcher"))


;; ===== Stores =====

(def store-state (atom {}))


(def store-service
  (-> (service "project-store")
      (install-implementation!
        (merge
          (create-implementation (create project/spec store-state))
          (create-implementation (create revision/spec store-state))
          (create-implementation (create unit/spec store-state))))
      (register-output! error-bus)
      (start-service!)))


(def store-bus
  (-> (bus "store-bus")
      (register-service! store-service)))


;; ===== Renderer =====

(def renderer-impl
  (merge
    app-a/actions
    proj-a/actions
    proj-list-a/actions))


(def renderer
  (-> (service "renderer")
      (register-output! error-bus)
      (install-implementation! renderer-impl)
      (start-service!)))


(def render-bus
  (-> (bus "render-bus")
      (register-service! renderer)))


;; ===== Mediator =====

(def mediator
  (-> (service "mediator")
      (install-implementation!
        (merge
          app-mediator
          project-mediator))
      (register-output! {:default render-bus
                         :dispatcher dispatcher-bus})
      (start-service!)))


(def mediator-bus
  (-> (bus "mediator-bus")
      (register-service! mediator)))


;; ===== Serializer =====

(def serializer
  (-> (service "serializer")
      (install-implementation!
        (merge
          revision-serializer
          project-serializer))
      (register-output! {:default error-bus
                        :store store-bus})
      (start-service!)))

(def serializer-bus
  (-> (bus "serializer-bus")
      (register-service! serializer)))


;; ===== Interactors =====

(def interactor
  (-> (service "interactor")
      (install-implementation!
        (merge
          proj-i/interactor
          app-i/interactor))
      (register-output! {:default mediator-bus
                         :serializer serializer-bus
                         :store store-bus})
      (start-service!)))


;; ===== Connections =====

(register-service! dispatcher-bus interactor)

(connect! mediator-bus (endpoint dispatcher-bus))
(connect! render-bus (endpoint mediator-bus))
(connect! error-bus (endpoint render-bus))
(connect! error-bus (endpoint serializer-bus))

