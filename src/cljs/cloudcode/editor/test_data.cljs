(ns cloudcode.editor.test-data)


(def test-project1
  {:name "proj1"
   :user-deps
   #{{:project-name "proj2"
      :revision-label "first rev"}
     {:project-name "proj3"
      :revision-label "version-1.2.3"}}})

(def test-revision1
  {:label "v-1.0.0"
   :project "proj1"
   :rev 5
   :interface
   #{{:path "proj1.utils.math.add"
      :source "function (a, b) {return a + b;}"}

     {:path "proj1.utils.math.addAll"
      :dependencies
      #{{:path "proj1.utils.math.add"
        :alias "add"}}
      :source "function (coll) {return coll.reduce(add, 0);}"}}})

(def test-revision2
  {:label "v-0.0.9"
   :project "proj1"
   :rev 3
   :interface
   #{{:path "proj1.utils.math.add"
      :source "function (A, B) {return A + B;}"}

     {:path "proj1.utils.math.addAll"
      :dependencies
      #{{:path "proj1.utils.math.add"
         :alias "add"}}
      :source "function (C) {return C.reduce(add, 0);}"}}})

(def test-revisions
  {:revisions
   #{test-revision1
     test-revision2}})

(def test-projects
  {:projects
   #{test-project1
     {:name "proj2"}
     {:name "proj3"}}})

(def test-data (merge test-revisions test-projects))
