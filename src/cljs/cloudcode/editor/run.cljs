(ns cloudcode.editor.run
  (:require
    [cloudcode.editor.infrastructure]
    [cloudcode.editor.test-data :refer [test-data]]
    [cloudcode.renderer.state :refer [app]]
    [cloudcode.renderer.dispatcher :refer [dispatch]]
    [cloudcode.renderer.views.app :as v]
    [cloudcode.utils.browser.dom :refer [by-id]]
    [cloudcode.utils.browser.console :refer [log]]
    [clojure.browser.repl]
    [om.core :as om :include-macros true]))


(om/root
  (fn [app owner] (om/component (v/app-root app)))
  app
  {:target (.-body js/document)})


(dispatch :app.renderer/update-layout :layout/dashboard)
(dispatch :app.action/import test-data)
