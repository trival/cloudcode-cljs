(ns cloudcode.server.service.db
  (:require [cloudcode.utils.data.datomic :refer [spec->schema id-schema datomic->model id-key]]
            [datomic.api :as d]
            [clojure.edn :as edn]
            [cloudcode.entities.project :as project]
            [cloudcode.entities.revision :as revision]
            [cloudcode.entities.session :as session]
            [cloudcode.entities.user :as user]
            [cloudcode.entities.unit :as unit]))


(def config
  (:database (edn/read-string (slurp "server-config.edn"))))


(defn create
  []
  (d/create-database (:uri config)))


(defn conn
  []
  (d/connect (:uri config)))


(defn db
  []
  (d/db (conn)))


(defn cloudcode-schemas
  []
  (concat id-schema
          (spec->schema project/spec)
          (spec->schema project/deps-spec)
          (spec->schema revision/spec)
          (spec->schema unit/spec)
          (spec->schema unit/deps-spec)
          (spec->schema user/spec)
          (spec->schema session/spec)))


(defn setup-db
  []
  (create)
  (d/transact (conn) (cloudcode-schemas)))


;; ===== helper methods =====

(defn e-by-id
  "returns the model refered by the id"
  [id]
  (d/entity (db) [id-key id]))


(defn q
  "Datomic query with db bound to local db"
  [query & args]
  (apply d/q query (db) args))


(defn transact
  "Datomic transact with conn bound to local connection"
  [tx-data]
  (d/transact (conn) tx-data))


(defn qe
  "Returns the single entity returned by a query."
  [query & args]
  (let [db (db)
        res (apply d/q query db args)]
    (d/entity db (ffirst res))))


(defn find-by
  "Returns the unique entity identified by attr and val."
  [attr val]
  (let [db (db)]
    (qe '[:find ?e
          :in $ ?attr ?val
          :where [?e ?attr ?val]]
        db (d/entid db attr) val)))


(defn qes
  "Returns the entities returned by a query, assuming that
  all :find results are entity ids."
  [query & args]
  (let [db (db)]
    (->> (apply d/q query (db) args)
         (mapv (fn [items]
                 (mapv (partial d/entity (db)) items))))))


(defn find-all-by
  "Returns all entities possessing attr."
  [attr]
  (let [db (db)]
    (qes '[:find ?e
           :in $ ?attr
           :where [?e ?attr]]
         db (d/entid db attr))))


(defn qfs
  "Returns the first of each query result."
  [query & args]
  (->> (apply d/q query (db) args)
       (mapv first)))



