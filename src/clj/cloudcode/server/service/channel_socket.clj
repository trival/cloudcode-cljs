(ns cloudcode.server.service.channel-socket
  (:require
    [cloudcode.utils.system.log :refer [log]]
    [taoensso.sente :as sente]))


(let [{:keys [ch-recv send-fn ajax-post-fn ajax-get-or-ws-handshake-fn
              connected-uids]}
      (sente/make-channel-socket! {})]

  (def ring-ajax-post ajax-post-fn)
  (def ring-ajax-get-or-ws-handshake ajax-get-or-ws-handshake-fn)
  (def chsk-recv ch-recv) ; ChannelSocket's receive channel
  (def chsk-send! send-fn) ; ChannelSocket's send API fn
  (def connected-uids connected-uids)) ; Watchable, read-only atom)


(defn format-req
  [chsk-req]
  (let [{:as ev-msg :keys [ring-req event ?reply-fn]} chsk-req
        session (:session ring-req)
        uid (:uid session)
        [id data] event]

    {:ring-req ring-req
     :reply-fn ?reply-fn
     :session session
     :uid uid
     :id id
     :data data}))


(defn valid-reply-fn?
  [reply-fn]
  (if (and (fn? reply-fn)
           (not (:dummy-reply-fn? (meta reply-fn))))
    true false))


(defn noop-handler
  [r]
  (log "noop on " (:id r))
  nil)


(defn action-with-user
  [action req]
  (if (:uid req)
    (action req)
    (let [reply (:reply-fn req)]
      (when (valid-reply-fn? reply)
        (log "no uid on request " (:id req))
        (reply [:alert/no-user-logged-in nil])))))


(defn unknown-route-handler
  [r]
  (let [{:keys [id reply-fn data]} r]
    (log "got unmatched socket request: " id)
    (when (valid-reply-fn? reply-fn)
      (reply-fn [:alert/umatched-event-as-echoed-from-from-server
                 [id data]]))))


(def default-routes
  {:chsk/ws-ping noop-handler
   :chsk/uidport-open noop-handler
   :chsk/uidport-close noop-handler})


(defn handler-from-routemap
  [routemap]
  (fn [sk-req] (let [req (format-req sk-req)
                     routes (merge default-routes routemap)
                     action (get routes (:id req) unknown-route-handler)]
                 (action-with-user action req))))


