(ns cloudcode.server.user.queries
  (:require
    [cloudcode.server.service.db :as db]
    [cloudcode.entities.user :refer [spec]]
    [cloudcode.utils.data.datomic :refer [model->datomic datomic->model with-tempid]]
    [cloudcode.utils.system.log :refer [log]]
    [datomic.api :as d]))


; ===== query methods =====

(defn get-user-by-id
  [id]
  (datomic->model spec (db/e-by-id id)))


(defn get-user-by-uid
  [uid]
  (datomic->model spec (d/entity (db/db) [:user/uid uid])))


(defn get-user-by-email
  [email]
  (datomic->model spec (d/entity (db/db) [:user/email email])))


(defn create-user
  [user]
  (try
    (let [tx (db/transact [(->> user
                                (model->datomic spec)
                                (with-tempid))])]
      @tx
      [nil user])
    (catch Exception e
      [:error.user-signup/save-failed e])))
