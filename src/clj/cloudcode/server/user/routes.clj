(ns cloudcode.server.user.routes
  (:require [cloudcode.server.user.actions :as a]
            [cloudcode.utils.system.log :refer [log]]
            [cloudcode.entities.user :refer [spec]]
            [cloudcode.utils.data.model :as m]
            [cloudcode.utils.ring.response :refer [edn-resp update-session]]
            [compojure.core :refer [GET POST defroutes]]))


;; ===== ajax routes =====

(defn- auth-response
  [session [error user-or-errordata]]
  (if error
    (-> {:error [error user-or-errordata]}
        (edn-resp)
        (update-session (assoc session :uid nil)))
    (let [user user-or-errordata]
      (-> (m/required-only spec user)
          (edn-resp)
          (update-session (assoc session :uid (:id user)))))))


(defn login
  [req]
  (auth-response (:session req) (a/login (get-in req [:params :user]))))


(defn signup
  [req]
  (log (:params req))
  (auth-response (:session req) (a/signup (get-in req [:params :user]))))


(defn logout
  [req]
  (let [{:keys [session]} req]
    (log "Logout request")
    (-> {:uid nil}
        (edn-resp)
        (update-session (assoc session :uid nil)))))


;; ===== socket routes =====

(defn get-current
  [{:keys [uid reply-fn]}]
  (reply-fn (m/required-only spec (a/get-user uid))))


;; ===== routes definitions =====

(defroutes http-routes
  (POST "/login" req (login req))
  (POST "/signup" req (signup req))
  (POST "/logout" req (logout req)))


(def socket-routes
  {:user/get-current get-current})

