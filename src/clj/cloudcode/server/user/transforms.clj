(ns cloudcode.server.user.transforms
  (:require
    [crypto.password.pbkdf2 :as pwd]))


(defn hash-password
  [user]
  (assoc user :passhash (pwd/encrypt (:password user))))
