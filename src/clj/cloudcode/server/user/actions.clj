(ns cloudcode.server.user.actions
  (:require
    [cloudcode.entities.user :refer [spec]]
    [cloudcode.server.user.queries :as q]
    [cloudcode.server.user.transforms :as t]
    [cloudcode.utils.data.model :as m]
    [crypto.password.pbkdf2 :as pwd]))


(defn login
  [data]
  ;; data should be a uid/password or email/password combination

  ;; check for a valid uid or valid email and fetch a user
  ;; from the db
  (let [user (or (and (:email data)
                      (m/valid-prop? spec data :email)
                      (q/get-user-by-email (:email data)))
                 (and (:uid data)
                      (m/valid-prop? spec data :uid)
                      (q/get-user-by-uid (:uid data))))]

    ;; if no user is found send error
    (if-not user
      [:error.user/not-found]
      ;; else check password
      (if-not (pwd/check (:password data) (:passhash user))
        [:error.user/incorrect-password]
        [nil user]))))


(defn signup
  [user]
  (let [user (m/create-of spec user)
        validation (m/validate-model spec user)]
    (cond
      (not (m/valid-results? validation))
      [:error.user-signup/invalid (m/false-validations validation)]

      (q/get-user-by-uid (:uid user))
      [:error.user-signup/duplicate-uid]

      (q/get-user-by-email (:email user))
      [:error.user-signup/duplicate-email]

      :else
      (-> user
          (t/hash-password)
          (q/create-user)))))


(defn get-user
  [id]
  (q/get-user-by-id id))

