(ns cloudcode.server.main
  (:require
    [cloudcode.server.service.channel-socket :as cs]
    [cloudcode.server.user.routes :as user-r]
    [cloudcode.server.unit.routes :as unit-r]
    [cloudcode.server.project.routes :as proj-r]
    [cloudcode.server.session.routes :as sess-r]
    [cloudcode.utils.system.log :refer [log]]
    [clojure.edn :as edn]
    [clojure.java.io :as io]
    [cemerick.austin.repls :refer [browser-connected-repl-js]]
    [net.cgrand.enlive-html :as enlive]
    [compojure.route :as route]
    [clojure.core.async :as async :refer (<! <!! >! >!! put! chan go go-loop)]
    [compojure.handler :refer [site]]
    [compojure.core :refer [GET POST defroutes]]
    [taoensso.sente :as sente]
    [ring.middleware.reload :as reload]
    [ring.middleware.edn :refer [wrap-edn-params]]
    [org.httpkit.server :as httpkit]))


(def config
  (:server (edn/read-string (slurp "server-config.edn"))))


(defroutes http-routes
  user-r/http-routes

  (GET  "/editorsocket" req (cs/ring-ajax-get-or-ws-handshake req))
  (POST "/editorsocket" req (cs/ring-ajax-post req))

  (route/resources "/")
  (route/not-found "<h1>Page not found</h1>"))


(def app (-> #'http-routes
             (site)))


;; ----- sente setup -----

(def socket-routes
  (merge
    user-r/socket-routes
    proj-r/socket-routes
    sess-r/socket-routes
    unit-r/socket-routes))


(def socket-req-handler
  (cs/handler-from-routemap socket-routes))


(go-loop [] (let [req (<! cs/chsk-recv)]
              (socket-req-handler req)
              (recur)))


;; ----- dev system with repl and server reload -----

(when (= :dev (:env config))

  (enlive/deftemplate page
    (io/resource "public/index.html")
    []
    [:body] (enlive/append
              (enlive/html [:script (browser-connected-repl-js)])))


  (defroutes dev-routes
    (GET  "/repl"  req (page))
    http-routes)


  (def dev-app
    (-> #'dev-routes
        (reload/wrap-reload)
        (wrap-edn-params)
        (site))))


;; ----- run server method -----

(defn run
  []
  (log "server running on port: " (:port config))
  (httpkit/run-server
    (if (= :dev (:env config))
      #'dev-app
      #'app)
    {:port (:port config)}))
