(ns cloudcode.utils.ring.response
  (:require
    [ring.util.response :refer [response content-type]]))


(defn edn-resp
  [data]
  (-> (pr-str data)
      (response)
      (content-type "application/edn")))


(defn update-session
  [resp session]
  (assoc resp :session session))
