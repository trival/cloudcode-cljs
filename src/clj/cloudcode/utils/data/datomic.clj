(ns cloudcode.utils.data.datomic
  (:require
    [clojure.edn :as edn]
    [datomic.api :as d]))


(def id-key :uuid/id)


(def db-types
  {:option :db.type/string
   :string :db.type/string
   :edn :db.type/string
   :float :db.type/float
   :long :db.type/long
   :boolean :db.type/boolean
   :ref :db.type/uuid
   :uuid :db.type/uuid
   :instant :db.type/instant})


;; ===== Schema creation =====

(defn basic-attribute
  []
  {:db/id (d/tempid :db.part/db)
   :db.install/_attribute :db.part/db})


(def id-schema
  [(merge (basic-attribute)
          {:db/unique :db.unique/identity
           :db/doc "global identifier application entities"
           :db/cardinality :db.cardinality/one
           :db/valueType :db.type/uuid
           :db/ident id-key})])


(defn attribute-key
  [e-name a-name]
  (if (= a-name "id")
    id-key
    (keyword (str e-name "/" a-name))))


(defn attribute-type
  [t]
  (if (map? t)
    :db.type/ref
    (db-types t)))


(defn check-doc
  [attr props]
  (if-let [doc (:doc props)]
    (assoc attr :db/doc doc)
    attr))


(defn check-unique
  [attr props]
  (if-let [unique (get-in props [:db :unique])]
    (condp = unique
      :value (assoc attr :db/unique :db.unique/value)
      :identity (assoc attr :db/unique :db.unique/identity)
      attr)
    attr))


(defn check-component
  [attr props]
  (if (get-in props [:db :component?])
    (assoc attr :db/isComponent true)
    attr))


(defn create-attribute
  [e-name a-name props]
  (let [t (:type props)
        attr-t (or (get-in props [:db :type])
                   (:item-type props)
                   t)]
    (-> (basic-attribute)
        (assoc :db/ident (attribute-key e-name a-name))
        (assoc :db/valueType (attribute-type attr-t))
        (assoc :db/cardinality (if (= :set t)
                                 :db.cardinality/many
                                 :db.cardinality/one))
        (check-doc props)
        (check-unique props)
        (check-component props))))


(defn spec->schema
  [spec]
  (->> (:properties spec)
       ;; remove properties that need to be skiped
       (remove (fn [[_ v]] (or (:identifier? v)
                               (get-in v [:db :skip]))))
       ;; create datomic attribute schema for each property
       (map (fn [[k v]]
              (create-attribute (:name spec) (name k) v)))
       (vec)))


;; ===== Data conversion =====

(defn db-only
  [spec model]
  (->> model
       (remove (fn [[k _]]
                 (get-in spec [:properties k :db :skip])))
       (into {})))


(declare model->datomic)

(defn model-type->datomic-type-transform
  [t item-t]
  (if (map? t)
    (partial model->datomic t)
    (condp = t
      :edn pr-str
      :uuid (fn [v] (if (= v :new) (d/squuid) v))
      :set #(->> %
                 (map (model-type->datomic-type-transform item-t nil))
                 (vec))
      identity)))


(defn model->datomic
  [spec model]
  (->> model
       (db-only spec)
       (map (fn [[k v]]
              (let [{:keys [type item-type]} (get-in spec [:properties k])
                    attr-key (attribute-key (:name spec) (name k))
                    attr-val ((model-type->datomic-type-transform type item-type) v)]
                [attr-key attr-val])))
       (into {})))


(declare datomic->model)

(defn datomic-type->model-type-transform
  [t item-t]
  (if (map? t)
    (partial datomic->model item-t)
    (condp = t
      :edn edn/read-string
      :set #(->> %
                 (map (datomic-type->model-type-transform item-t nil))
                 (set))
      identity)))


(defn datomic->model
  [spec data]
  (when data
    (->> (d/touch data)
         (map (fn [[k v]]
                (let [{:keys [type item-type]} (get-in spec [:properties k])
                      attr-key (keyword (name k))
                      attr-val ((datomic-type->model-type-transform type item-type) v)]
                  [attr-key attr-val])))
         (into {}))))


(defn with-tempid
  [model]
  (assoc model :db/id (d/tempid :db.part/user)))


