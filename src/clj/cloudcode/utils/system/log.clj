(ns cloudcode.utils.system.log)


(defn log
  [& args]
  (.println System/out (apply str args)))
